class AddDeletedAtToClanMembers < ActiveRecord::Migration
  def change
    add_column :clan_members, :deleted_at, :datetime
    add_index :clan_members, :deleted_at
  end
end
