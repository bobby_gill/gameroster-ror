class AddWasFullToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :was_full, :boolean, default: false
  end
end
