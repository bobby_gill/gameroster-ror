class AddDetailsToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :details, :text
  end
end
