class CreateClanMessages < ActiveRecord::Migration
  def change
    create_table :clan_messages do |t|
      t.integer :user_id
      t.string :message
      t.integer :clan_id
    end
  end
end
