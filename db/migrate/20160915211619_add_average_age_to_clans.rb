class AddAverageAgeToClans < ActiveRecord::Migration
  def change
    add_column :clans, :average_age, :integer
  end
end
