class AddTimesToClanMessages < ActiveRecord::Migration
  def change
    add_column :clan_messages, :created_at, :datetime
    add_column :clan_messages, :updated_at, :datetime
  end
end
