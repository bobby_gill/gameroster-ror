class CreateClanInvites < ActiveRecord::Migration
  def change
    create_table :clan_invites do |t|
      t.references :user, index: true, foreign_key: true
      t.references :clan, index: true, foreign_key: true
      t.integer :inviter_id, index: true
      t.integer :status

      t.timestamps
    end

    add_foreign_key :clan_invites, :users, column: :inviter_id
  end
end
