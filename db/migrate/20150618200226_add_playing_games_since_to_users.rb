class AddPlayingGamesSinceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :playing_games_since, :string
  end
end
