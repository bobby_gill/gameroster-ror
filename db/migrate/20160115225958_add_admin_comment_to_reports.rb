class AddAdminCommentToReports < ActiveRecord::Migration
  def change
    add_column :reports, :admin_comment, :text
  end
end
