class CreateClanGames < ActiveRecord::Migration
  def change
    create_table :clan_games do |t|
      t.integer :game_id
      t.integer :clan_id

      t.timestamps null: false
    end
  end
end
