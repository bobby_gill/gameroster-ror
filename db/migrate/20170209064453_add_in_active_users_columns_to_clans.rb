class AddInActiveUsersColumnsToClans < ActiveRecord::Migration
  def change
    add_column :clans, :remove_inactive_users, :string
  end
end
