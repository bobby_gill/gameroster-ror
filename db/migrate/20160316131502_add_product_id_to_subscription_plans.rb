class AddProductIdToSubscriptionPlans < ActiveRecord::Migration
  def change
    add_column :subscription_plans, :product_id, :string
  end
end
