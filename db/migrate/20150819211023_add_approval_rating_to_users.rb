class AddApprovalRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :approval_rating, :integer
  end
end
