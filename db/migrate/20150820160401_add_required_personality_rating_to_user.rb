class AddRequiredPersonalityRatingToUser < ActiveRecord::Migration
  def change
    add_column :users, :required_personality_rating, :integer
  end
end
