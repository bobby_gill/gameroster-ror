class CreateClanApplications < ActiveRecord::Migration
  def change
    create_table :clan_applications do |t|
      t.belongs_to :clan, index: true
      t.belongs_to :user, index: true
      t.datetime   :deleted_at, index: true
      t.timestamps null: false
    end
    add_column :questions, :deleted_at, :datetime
    add_index :questions, :deleted_at
  end
end
