class AddLevelToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :level, :string
  end
end
