class AddWillPlayToUsers < ActiveRecord::Migration
  def change
    add_column :users, :will_play, :string
  end
end
