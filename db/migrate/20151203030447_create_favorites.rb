class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :user, index: true, foreign_key: true
      t.references :favorited_user, references: :users, index: true

      t.timestamps null: false

    end
    
    add_foreign_key :favorites, :users, column: :favorited_user_id, primary_key: "id"
  end
end
