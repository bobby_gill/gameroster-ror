class AddFeedbackTimesToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_feedback_date_time, :datetime
    add_column :contracts, :seller_feedback_date_time, :datetime
  end
end
