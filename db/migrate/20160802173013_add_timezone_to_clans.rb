class AddTimezoneToClans < ActiveRecord::Migration
  def change
    add_column :clans, :timezone, :string
  end
end
