class CreateContractGameGameSystemJoins < ActiveRecord::Migration
  def change
    create_table :contract_game_game_system_joins do |t|
      t.references :contract
      t.references :game_game_system_join

      t.timestamps null: false
    end
  end
end
