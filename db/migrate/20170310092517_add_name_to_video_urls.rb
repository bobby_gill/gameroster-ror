class AddNameToVideoUrls < ActiveRecord::Migration
  def change
    add_column :video_urls, :name, :string
  end
end
