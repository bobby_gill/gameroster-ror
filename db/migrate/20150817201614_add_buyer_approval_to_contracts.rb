class AddBuyerApprovalToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_approval, :integer
  end
end
