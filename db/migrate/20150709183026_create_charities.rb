class CreateCharities < ActiveRecord::Migration
  def change
    create_table :charities do |t|
      t.string :charity_name
      t.string :charity_logo
      t.string :charity_about
      t.string :charity_url

      t.timestamps null: false
    end
  end
end
