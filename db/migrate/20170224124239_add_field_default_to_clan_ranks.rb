class AddFieldDefaultToClanRanks < ActiveRecord::Migration
  def change
    add_column :clan_ranks, :is_default, :boolean, default: false
  end
end
