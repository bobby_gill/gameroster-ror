class RemoveStatusFromClanApplications < ActiveRecord::Migration
  def change
    remove_column :clan_applications, :status
  end
end
