class AlterColumnClanGameType < ActiveRecord::Migration
  def self.up
  	change_table :clans do |t|
  		t.change :game_type, :string
  	end
  end
  def self.down
  	change_table :clans do |t|
  		t.change :game_type, :integer
  	end
  end
end
