class AddRequiredCancellationRateToUser < ActiveRecord::Migration
  def change
    add_column :users, :required_cancellation_rate, :integer
  end
end
