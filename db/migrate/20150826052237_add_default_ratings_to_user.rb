class AddDefaultRatingsToUser < ActiveRecord::Migration
  def change
    change_column :users, :personality_rating, :integer, :default => 0
    change_column :users, :approval_rating, :integer, :default => 0
    change_column :users, :skill_rating, :integer, :default => 0
    change_column :users, :psa_rating, :integer, :default => 0
    change_column :users, :required_personality_rating, :integer, :default => 0
    change_column :users, :required_approval_rating, :integer, :default => 0
    change_column :users, :required_skill_rating, :integer, :default => 0
    change_column :users, :required_psa_rating, :integer, :default => 0
  end
end
