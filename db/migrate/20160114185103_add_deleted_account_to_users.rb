class AddDeletedAccountToUsers < ActiveRecord::Migration
  def change
		add_column :users, :deleted_account, :boolean, :default => false
  end
end
