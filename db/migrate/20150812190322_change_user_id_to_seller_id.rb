class ChangeUserIdToSellerId < ActiveRecord::Migration
  def change
	  rename_column :contracts, :user_id, :seller_id
  end
end
