class CreateRosterMessages < ActiveRecord::Migration
  def change
    create_table :roster_messages do |t|
        t.integer :user_id
        t.text :message
        t.integer :roster_message_board_id
        t.timestamps null: false
    end
  end
end
