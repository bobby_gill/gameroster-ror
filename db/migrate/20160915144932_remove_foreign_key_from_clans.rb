class RemoveForeignKeyFromClans < ActiveRecord::Migration
  def change
    remove_foreign_key :clans, column: :default_rank_id
  end
end
