class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :rated_user, index: true, foreign_key: false, null: false
      t.integer :personality_rating, default: 0
      t.integer :approval_rating, default: 0
      t.integer :skill_rating, default: 0
      t.text :comment, default: ""

      t.timestamps null: false
    end

    add_foreign_key :ratings, :users, column: :rated_user_id
    add_index :ratings, [:user_id, :rated_user_id], unique: true

  end
end
