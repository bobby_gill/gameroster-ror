class ChangeHandledAtToDateInReports < ActiveRecord::Migration
  def change
    remove_column :reports, :handled_at
    add_column :reports, :handled_at, :datetime
  end
end
