class AddRequiredPsaRatingToUser < ActiveRecord::Migration
  def change
    add_column :users, :required_psa_rating, :integer
  end
end
