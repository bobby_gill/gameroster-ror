class AddSellerSkillToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :seller_skill, :integer
  end
end
