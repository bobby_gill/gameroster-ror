class AddCancellationReasonToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :cancellation_reason, :text
  end
end
