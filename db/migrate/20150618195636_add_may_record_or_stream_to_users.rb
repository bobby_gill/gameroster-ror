class AddMayRecordOrStreamToUsers < ActiveRecord::Migration
  def change
    add_column :users, :may_record_or_stream, :string
  end
end
