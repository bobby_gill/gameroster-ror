class CreateClanGameSystems < ActiveRecord::Migration
  def change
    create_table :clans_game_systems, id: false do |t|
      t.belongs_to  :clan, index: true
      t.belongs_to  :game_system, index: true
    end
  end
end
