class AddContractsCompletedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :contracts_completed, :integer, default: 0
    add_index :users, :contracts_completed
  end
end
