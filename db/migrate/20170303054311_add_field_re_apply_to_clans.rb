class AddFieldReApplyToClans < ActiveRecord::Migration
  def change
    add_column :clans, :re_apply, :boolean, default: true
  end
end
