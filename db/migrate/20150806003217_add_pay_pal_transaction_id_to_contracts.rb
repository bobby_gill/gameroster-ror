class AddPayPalTransactionIdToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :pay_pal_transaction_id, :string
  end
end
