class ChangeColumnNameInClans < ActiveRecord::Migration
  def change
    rename_column :clans, :game_systems, :clan_game_systems
  end
end
