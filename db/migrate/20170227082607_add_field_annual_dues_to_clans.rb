class AddFieldAnnualDuesToClans < ActiveRecord::Migration
  def change
    add_column :clans, :annual_dues, :integer, default: 0
    add_column :clans, :annual_dues_amount, :integer, default: 0
  end
end
