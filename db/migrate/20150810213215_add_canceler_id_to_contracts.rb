class AddCancelerIdToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :canceler_id, :integer
  end
end
