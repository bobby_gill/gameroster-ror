class AddBuyerIdToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_id, :integer
  end
end
