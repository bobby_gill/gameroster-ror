class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :donatee, index: true, null: false
      t.integer :amount_cents, null: false
      t.integer :status, default: 0, null: false
      t.string  :pay_pal_transaction_id

      t.timestamps null: false
    end
    add_index :donations, :status
    add_index :donations, :pay_pal_transaction_id
    add_foreign_key :donations, :users, column: :donatee_id, primary_key: :id
  end
end
