class CreateClanMembers < ActiveRecord::Migration
  def change
    add_column :clans, :host_id, :integer

    add_column :clans, :private, :boolean, default: false

    create_table :clan_members do |t|
      t.belongs_to :clan, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
