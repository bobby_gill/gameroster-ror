class AddPlayingForCharityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :playing_for_charity, :string
  end
end
