class AlterColumnClanActivityLevel < ActiveRecord::Migration
  def self.up
  	change_table :clans do |t|
  		t.change :activity_level, :string
  	end
  end
  def self.down
  	change_table :clans do |t|
  		t.change :activity_level, :integer
  	end
  end
end
