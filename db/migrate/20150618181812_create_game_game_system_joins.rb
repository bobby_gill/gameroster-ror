class CreateGameGameSystemJoins < ActiveRecord::Migration
  def change
    create_table :game_game_system_joins do |t|
      t.integer :game_id
      t.integer :game_system_id

      t.timestamps null: false
    end
  end
end
