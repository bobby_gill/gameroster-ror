class CreateClansGames < ActiveRecord::Migration
  def change
    create_table :clans_games, id: false do |t|
      t.belongs_to :clan, index: true
      t.belongs_to :game, index: true
    end
  end
end
