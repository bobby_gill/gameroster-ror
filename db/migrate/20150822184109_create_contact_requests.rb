class CreateContactRequests < ActiveRecord::Migration
  def change
    create_table :contact_requests do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :contact_type
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
