class AddSystemAvatarToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :system_avatar, index: true
  end
end
