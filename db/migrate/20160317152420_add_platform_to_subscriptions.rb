class AddPlatformToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :platform, :integer, default: 0, null: false
  end
end
