class AddSellerPersonalityToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :seller_personality, :integer
  end
end
