class AddPsaRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :psa_rating, :integer
  end
end
