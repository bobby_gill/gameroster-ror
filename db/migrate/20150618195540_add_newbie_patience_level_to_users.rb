class AddNewbiePatienceLevelToUsers < ActiveRecord::Migration
  def change
    add_column :users, :newbie_patience_level, :string
  end
end
