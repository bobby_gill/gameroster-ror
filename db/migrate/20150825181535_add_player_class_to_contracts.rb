class AddPlayerClassToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :player_class, :string
  end
end
