class CreateVideoUrls < ActiveRecord::Migration
  def change
    create_table :video_urls do |t|
      t.string :url
      t.integer :clan_id

      t.timestamps null: false
    end
  end
end
