class AddGameMercsPayPalTransactionIdToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :game_mercs_pay_pal_transaction_id, :string
  end
end
