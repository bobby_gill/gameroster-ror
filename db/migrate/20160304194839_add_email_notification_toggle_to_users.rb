class AddEmailNotificationToggleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notif_email, :boolean, default: true
  end
end
