class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.references :user, index: true, foreign_key: true
      t.references :contract, index: true, foreign_key: true
      t.references :blocked_user, references: :users, index: true

      t.timestamps null: false

    end
    
    add_foreign_key :blocks, :users, column: :blocked_user_id, primary_key: "id"
  end
end
