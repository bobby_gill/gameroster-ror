class AddCancellationAssigneeToContracts < ActiveRecord::Migration
  def change
    add_reference :contracts, :cancellation_assignee, references: :users, index: true
  end
end
