class AddPlayTypeToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :play_type, :string
  end
end
