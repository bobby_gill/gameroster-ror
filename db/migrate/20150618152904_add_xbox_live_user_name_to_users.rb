class AddXboxLiveUserNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :xbox_live_user_name, :string
  end
end
