class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.belongs_to :clan, index: true
      t.string :name
      t.timestamps null: false
    end
  end
end
