class AddBuyerPersonalityToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_personality, :integer
  end
end
