class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.belongs_to :user, index: true
      t.belongs_to :clan, index: true
      t.belongs_to :question, index: true
      t.belongs_to :clan_application, index: true
      t.string :answer

      t.timestamps null: false
    end
  end
end
