class ChangeDefaultNotifGameOnUsersToOptIn < ActiveRecord::Migration
  def down
    change_column_default :users, :notif_games, false
    execute "update users set notif_games = false;"
  end

  def up
    change_column_default :users, :notif_games, true
    execute "update users set notif_games = true;"
  end
end
