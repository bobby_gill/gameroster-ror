class CreateDonationPaypalLogs < ActiveRecord::Migration
  def change
    create_table :donation_paypal_logs do |t|
      t.references :donation, index: true, foreign_key: true
      t.text :log

      t.timestamps null: false
    end
  end
end
