class AddRequirementsToClans < ActiveRecord::Migration
  def change
    add_column :clans, :requirements, :string
  end
end
