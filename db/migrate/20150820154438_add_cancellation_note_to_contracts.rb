class AddCancellationNoteToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :cancellation_note, :text
  end
end
