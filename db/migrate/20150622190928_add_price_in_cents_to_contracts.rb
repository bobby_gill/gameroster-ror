class AddPriceInCentsToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :price_in_cents, :integer
  end
end
