class AddFieldStatusToClanApplications < ActiveRecord::Migration
  def change
    add_column :clan_applications, :status, :boolean, default: false
  end
end
