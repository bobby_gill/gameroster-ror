class AddLinksToClan < ActiveRecord::Migration
  def change
    create_table :clan_ranks do |t|
      t.references :clan, index: true, foreign_key: true
      t.integer :permissions
      t.string :title

    end

    add_column :clans, :default_rank_id, :integer
    add_reference :clan_members, :clan_rank, index: true, foreign_key: true

    add_column :clans, :twitch, :string
    add_column :clans, :youtube, :string
    add_column :clans, :facebook, :string
    add_column :clans, :twitter, :string

    add_foreign_key :clans, :clan_ranks, column: :default_rank_id
  end
end
