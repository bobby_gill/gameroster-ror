class AddInfoToClans < ActiveRecord::Migration
  def change
    add_column :clans, :avatar_url, :string
    add_column :clans, :motto, :string
    add_column :clans, :country, :string
    add_column :clans, :zip, :string
  end
end
