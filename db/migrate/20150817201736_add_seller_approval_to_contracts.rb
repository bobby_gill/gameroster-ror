class AddSellerApprovalToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :seller_approval, :integer
  end
end
