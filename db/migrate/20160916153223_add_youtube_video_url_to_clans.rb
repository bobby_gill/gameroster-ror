class AddYoutubeVideoUrlToClans < ActiveRecord::Migration
  def change
    add_column :clans, :youtube_video_url, :string
  end
end
