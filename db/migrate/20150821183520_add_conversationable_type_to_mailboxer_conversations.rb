class AddConversationableTypeToMailboxerConversations < ActiveRecord::Migration
  def change
    add_column :mailboxer_conversations, :conversationable_type, :string
  end
end
