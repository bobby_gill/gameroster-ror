class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references :user, index: true
      t.string :device_token
      t.string :device_type

      t.timestamps null: false
    end
    add_foreign_key :devices, :users
  end
end
