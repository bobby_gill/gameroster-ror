class AddMaxRosterSizeToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :max_roster_size, :integer
  end
end
