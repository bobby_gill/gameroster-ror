class AddSkillRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :skill_rating, :integer
  end
end
