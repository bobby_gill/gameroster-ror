class AddMostActiveDaysToClans < ActiveRecord::Migration
  def change
    add_column :clans, :most_active_days, :string
  end
end
