class CreateGameSystems < ActiveRecord::Migration
  def change
    create_table :game_systems do |t|
      t.string :title
      t.string :abbreviation

      t.timestamps null: false
    end
  end
end
