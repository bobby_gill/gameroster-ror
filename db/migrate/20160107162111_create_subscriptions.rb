class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :subscription_plan, index: true, foreign_key: true, null: false
      t.integer :state, default: 0, null: false
      t.date :ends_on, null: false
      t.string :token, null: false
      t.string :profile_id

      t.timestamps null: false
    end
  end
end
