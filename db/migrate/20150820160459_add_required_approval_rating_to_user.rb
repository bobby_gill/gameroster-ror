class AddRequiredApprovalRatingToUser < ActiveRecord::Migration
  def change
    add_column :users, :required_approval_rating, :integer
  end
end
