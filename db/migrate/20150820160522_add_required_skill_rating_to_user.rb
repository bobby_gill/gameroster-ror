class AddRequiredSkillRatingToUser < ActiveRecord::Migration
  def change
    add_column :users, :required_skill_rating, :integer
  end
end
