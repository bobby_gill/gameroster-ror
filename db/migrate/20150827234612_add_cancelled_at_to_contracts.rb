class AddCancelledAtToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :cancelled_at, :datetime
  end
end
