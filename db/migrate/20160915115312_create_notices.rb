class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.integer :user_id
      t.integer :admin_id
      t.integer :clan_id
      t.string :type
      t.string :title
      t.text :body
      t.datetime :expiration
      t.timestamps null: false

      t.index :clan_id
    end
  end
end
