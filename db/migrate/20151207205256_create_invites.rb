class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.references :contract, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :status, default: 0

      t.timestamps null: false
    end

    add_index :invites, [:contract_id, :user_id], unique: true

  end
end
