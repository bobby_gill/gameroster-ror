class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer "user_id"
      t.integer "commenter_id"
      t.text "comment"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
    end
  end
end
