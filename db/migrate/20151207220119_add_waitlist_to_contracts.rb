class AddWaitlistToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :waitlist, :boolean
  end
end
