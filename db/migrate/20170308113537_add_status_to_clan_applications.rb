class AddStatusToClanApplications < ActiveRecord::Migration
  def change
    add_column :clan_applications, :status, :boolean
    add_column :clan_applications, :reviewer_id, :integer
    add_column :clan_applications, :reviewed_at, :datetime

  end
end
