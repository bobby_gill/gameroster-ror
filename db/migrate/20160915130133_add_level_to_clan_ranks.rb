class AddLevelToClanRanks < ActiveRecord::Migration
  def change
    add_column :clan_ranks, :level, :integer
  end
end
