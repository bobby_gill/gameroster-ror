class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :reportable_id
      t.string :reportable_type
      t.references :user, index: true, foreign_key: true
      t.text :comment
      t.string :handled_at
      t.references :admin, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
