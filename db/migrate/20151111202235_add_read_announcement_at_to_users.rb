class AddReadAnnouncementAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :read_announcement_at, :datetime
  end
end
