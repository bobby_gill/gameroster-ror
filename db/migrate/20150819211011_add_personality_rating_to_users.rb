class AddPersonalityRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :personality_rating, :integer
  end
end
