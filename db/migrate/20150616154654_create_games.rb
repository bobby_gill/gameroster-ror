class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :title
      t.string :game_cover
      t.string :game_jumbo
      t.string :game_logo

      t.timestamps null: false
    end
  end
end
