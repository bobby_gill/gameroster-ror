class CreateContractPaypalLogs < ActiveRecord::Migration
  def change
    create_table :contract_paypal_logs do |t|
      t.integer :contract_id
      t.text :log

      t.timestamps null: false
    end
  end
end
