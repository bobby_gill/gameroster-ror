class AddConversationableIdToMailboxerConversations < ActiveRecord::Migration
  def change
    add_column :mailboxer_conversations, :conversationable_id, :integer
  end
end
