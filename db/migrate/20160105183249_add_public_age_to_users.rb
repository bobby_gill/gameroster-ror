class AddPublicAgeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :public_age, :boolean
  end
end
