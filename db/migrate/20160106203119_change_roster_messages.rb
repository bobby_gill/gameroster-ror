class ChangeRosterMessages < ActiveRecord::Migration
  def change
      remove_column :roster_messages, :roster_message_board_id
      add_column :roster_messages, :roster_id, :integer
  end
end
