class AddColumnsToClans < ActiveRecord::Migration
  def change
    add_column :clans, :game_systems, :string
    add_column :clans, :paypal_email, :string
  end
end
