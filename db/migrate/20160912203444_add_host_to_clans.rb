class AddHostToClans < ActiveRecord::Migration
  def change
    add_foreign_key "clans", "users", column: "host_id"
  end
end
