class AddGroundRulesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ground_rules, :text
  end
end
