class CreateClans < ActiveRecord::Migration
  def change
    create_table :clans do |t|
      t.string :name
      t.integer :activity_level
      t.integer :game_type
      t.integer :most_active
      t.string :game_system
      t.string :play_style
      t.string :languages
      t.string :bio
      t.string :age_restriction
      t.boolean :family_friendly
      t.boolean :availability
      t.string :ground_rules

      t.timestamps null: false
    end
  end
end
