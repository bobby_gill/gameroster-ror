class CreateSubscriptionPlans < ActiveRecord::Migration
  def change
    create_table :subscription_plans do |t|
      t.string :name
      t.decimal :price
      t.boolean :recurring
      t.integer :period

      t.timestamps null: false
    end
  end
end
