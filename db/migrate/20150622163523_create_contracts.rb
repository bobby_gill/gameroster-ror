class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.datetime :start_date_time
      t.integer :duration

      t.timestamps null: false
    end
  end
end
