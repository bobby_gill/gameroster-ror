class AddEndDateTimeToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :end_date_time, :datetime
  end
end
