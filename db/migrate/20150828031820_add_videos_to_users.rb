class AddVideosToUsers < ActiveRecord::Migration
  def change
    add_column :users, :twitch_video_url, :string
    add_column :users, :youtube_video_url, :string
  end
end
