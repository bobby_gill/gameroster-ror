class AddNotifReminderToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notif_reminder, :boolean, default: true
  end
end
