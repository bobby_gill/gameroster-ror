class CreateClanDonations < ActiveRecord::Migration
  def change
    create_table :clan_donations do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :clan, index: true, null: false, foreign_key: true
      t.integer :amount_cents, null: false
      t.integer :status, default: 0, null: false
      t.string  :pay_pal_transaction_id
      t.timestamps null: false
    end
    add_index :clan_donations, :status
    add_index :clan_donations, :pay_pal_transaction_id
    add_column :donation_paypal_logs, :clan_donation_id, :integer
  end
end
