class AddApproveInviteClans < ActiveRecord::Migration
  def change
    add_column :clans, :autojoin, :boolean, default: false
  end
end
