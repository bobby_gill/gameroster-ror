class AddPositionToInvites < ActiveRecord::Migration
  def change
    add_column :invites, :position, :integer
  end
end
