class AddGenerosityRatingToUser < ActiveRecord::Migration
  def change
    add_column :users, :generosity_rating, :integer, default: 0
  end
end
