class AddClaimedAtToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :claimed_at, :datetime
  end
end
