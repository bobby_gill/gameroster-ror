class RemoveClanGameSystemsFromClan < ActiveRecord::Migration
  def change
    remove_column :clans, :clan_game_systems, :string
  end
end
