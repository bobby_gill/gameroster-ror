class AddWillPlayToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :will_play, :string
  end
end
