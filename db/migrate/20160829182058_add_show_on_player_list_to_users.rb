class AddShowOnPlayerListToUsers < ActiveRecord::Migration
  def change
    add_column :users, :show_on_playerlist, :boolean, :default => true
  end
end
