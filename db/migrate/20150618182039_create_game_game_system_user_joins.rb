class CreateGameGameSystemUserJoins < ActiveRecord::Migration
  def change
    create_table :game_game_system_user_joins do |t|
      t.integer :game_game_system_join_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
