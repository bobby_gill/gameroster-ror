class AddTrialExpirationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :trial_expiration, :date
  end
end
