class AddNotifSiteToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notif_site, :boolean, null: false, default: true
  end
end
