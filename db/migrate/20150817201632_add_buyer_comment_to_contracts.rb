class AddBuyerCommentToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_comment, :text
  end
end
