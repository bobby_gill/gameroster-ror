class AddLanguageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :language, :text
  end
end
