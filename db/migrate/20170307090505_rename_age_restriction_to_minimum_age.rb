class RenameAgeRestrictionToMinimumAge < ActiveRecord::Migration
  def change
    rename_column :clans, :age_restriction, :minimum_age
  end
end
