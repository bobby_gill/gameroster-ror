class RemoveGameSystemFromClans < ActiveRecord::Migration
  def up
  	remove_column :clans, :game_system, :string
  end

  def down
  	add_column :clans, :game_system, :string
  end
end
