class AddPcAndNintendoIgnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pc_user_name, :string
    add_column :users, :nintendo_user_name, :string
  end
end
