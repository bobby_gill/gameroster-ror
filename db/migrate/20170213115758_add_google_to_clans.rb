class AddGoogleToClans < ActiveRecord::Migration
  def change
    add_column :clans, :google, :string
    add_column :clans, :instagram, :string
    add_column :clans, :curse, :string
    add_column :clans, :patreon, :string
    add_column :clans, :steam, :string
    add_column :clans, :reddit, :string
    add_column :clans, :mlg, :string
    add_column :clans, :status, :string
  end
end
