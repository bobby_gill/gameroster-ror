class AddDiscordInvitationToClans < ActiveRecord::Migration
  def change
    add_column :clans, :discord_invitation, :string, after: :discord
    add_column :clans, :curse_invitation, :string, after: :curse
    add_column :clans, :wargaming, :string, after: :mlg
    add_column :clans, :battle, :string, after: :wargaming
    add_column :clans, :skype, :string, after: :battle    
  end
end
