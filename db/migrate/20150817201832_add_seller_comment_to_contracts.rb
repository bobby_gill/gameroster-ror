class AddSellerCommentToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :seller_comment, :text
  end
end
