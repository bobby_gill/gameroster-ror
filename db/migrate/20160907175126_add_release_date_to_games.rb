class AddReleaseDateToGames < ActiveRecord::Migration
  def change
    add_column :games, :release_date, :datetime
  end
end
