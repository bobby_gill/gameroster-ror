class AddBuyerSkillToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :buyer_skill, :integer
  end
end
