class AddPrivateToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :private, :boolean
  end
end
