class AddNotificationsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notif_system, :boolean, default: true
    add_column :users, :notif_push, :boolean, default: true
    add_column :users, :notif_sms, :boolean, default: true
    add_column :users, :notif_games, :boolean, default: true
  end
end
