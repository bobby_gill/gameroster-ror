class AddClanIdToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :clan_id, :integer
  end
end
