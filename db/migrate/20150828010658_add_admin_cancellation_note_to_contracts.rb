class AddAdminCancellationNoteToContracts < ActiveRecord::Migration
  def change
    add_column :contracts, :admin_cancellation_note, :text
  end
end
