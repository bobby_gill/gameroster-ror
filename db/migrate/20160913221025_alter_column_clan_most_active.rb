class AlterColumnClanMostActive < ActiveRecord::Migration
  def self.up
  	change_table :clans do |t|
  		t.change :most_active, :string
  	end
  end
  def self.down
  	change_table :clans do |t|
  		t.change :most_active, :integer
  	end
  end
end
