class CreateSystemAvatars < ActiveRecord::Migration
  def change
    create_table :system_avatars do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
