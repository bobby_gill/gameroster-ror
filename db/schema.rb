# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170217105128) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "announcements", force: :cascade do |t|
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blocks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "contract_id"
    t.integer  "blocked_user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "blocks", ["blocked_user_id"], name: "index_blocks_on_blocked_user_id", using: :btree
  add_index "blocks", ["contract_id"], name: "index_blocks_on_contract_id", using: :btree
  add_index "blocks", ["user_id"], name: "index_blocks_on_user_id", using: :btree

  create_table "charities", force: :cascade do |t|
    t.string   "charity_name"
    t.string   "charity_logo"
    t.string   "charity_about"
    t.string   "charity_url"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "clan_games", force: :cascade do |t|
    t.integer  "game_id"
    t.integer  "clan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clan_invites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "clan_id"
    t.integer  "inviter_id"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "clan_invites", ["clan_id"], name: "index_clan_invites_on_clan_id", using: :btree
  add_index "clan_invites", ["inviter_id"], name: "index_clan_invites_on_inviter_id", using: :btree
  add_index "clan_invites", ["user_id"], name: "index_clan_invites_on_user_id", using: :btree

  create_table "clan_members", force: :cascade do |t|
    t.integer  "clan_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "clan_rank_id"
  end

  add_index "clan_members", ["clan_id"], name: "index_clan_members_on_clan_id", using: :btree
  add_index "clan_members", ["clan_rank_id"], name: "index_clan_members_on_clan_rank_id", using: :btree
  add_index "clan_members", ["user_id"], name: "index_clan_members_on_user_id", using: :btree

  create_table "clan_messages", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "message"
    t.integer  "clan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clan_ranks", force: :cascade do |t|
    t.integer "clan_id"
    t.integer "permissions"
    t.string  "title"
    t.integer "level"
  end

  add_index "clan_ranks", ["clan_id"], name: "index_clan_ranks_on_clan_id", using: :btree

  create_table "clans", force: :cascade do |t|
    t.string   "name"
    t.string   "activity_level"
    t.string   "game_type"
    t.string   "most_active"
    t.string   "play_style"
    t.string   "languages"
    t.string   "bio"
    t.string   "age_restriction"
    t.boolean  "family_friendly"
    t.boolean  "availability"
    t.string   "ground_rules"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "jumbo"
    t.string   "mobile_jumbo"
    t.string   "cover"
    t.string   "timezone"
    t.string   "avatar_url"
    t.string   "motto"
    t.string   "country"
    t.string   "zip"
    t.integer  "host_id"
    t.boolean  "private",               default: false
    t.boolean  "autojoin",              default: false
    t.integer  "default_rank_id"
    t.string   "twitch"
    t.string   "youtube"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "bungie"
    t.integer  "average_age"
    t.string   "youtube_video_url"
    t.string   "requirements"
    t.string   "discord"
    t.string   "most_active_days"
    t.string   "clan_game_systems"
    t.string   "paypal_email"
    t.string   "remove_inactive_users"
    t.string   "google"
    t.string   "instagram"
    t.string   "curse"
    t.string   "patreon"
    t.string   "steam"
    t.string   "reddit"
    t.string   "mlg"
    t.string   "status"
  end

  create_table "contact_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "contact_type"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "contact_requests", ["user_id"], name: "index_contact_requests_on_user_id", using: :btree

  create_table "contract_game_game_system_joins", force: :cascade do |t|
    t.integer  "contract_id"
    t.integer  "game_game_system_join_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "contract_paypal_logs", force: :cascade do |t|
    t.integer  "contract_id"
    t.text     "log"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "contracts", force: :cascade do |t|
    t.datetime "start_date_time"
    t.integer  "duration"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "price_in_cents"
    t.integer  "buyer_id"
    t.string   "status"
    t.integer  "seller_id"
    t.integer  "selected_game_game_system_join_id"
    t.string   "pay_pal_transaction_id"
    t.string   "game_mercs_pay_pal_transaction_id"
    t.text     "cancellation_reason"
    t.integer  "canceler_id"
    t.string   "contract_type"
    t.integer  "buyer_personality"
    t.integer  "buyer_skill"
    t.integer  "buyer_approval"
    t.text     "buyer_comment"
    t.integer  "seller_personality"
    t.integer  "seller_skill"
    t.integer  "seller_approval"
    t.text     "seller_comment"
    t.text     "cancellation_note"
    t.string   "level"
    t.string   "mission"
    t.text     "details"
    t.string   "player_class"
    t.datetime "end_date_time"
    t.datetime "buyer_feedback_date_time"
    t.datetime "seller_feedback_date_time"
    t.integer  "cancellation_assignee_id"
    t.datetime "cancelled_at"
    t.text     "admin_cancellation_note"
    t.datetime "claimed_at"
    t.string   "will_play"
    t.integer  "max_roster_size"
    t.boolean  "waitlist"
    t.string   "play_type"
    t.boolean  "private"
    t.boolean  "was_full",                          default: false
    t.string   "title"
    t.integer  "clan_id"
  end

  add_index "contracts", ["cancellation_assignee_id"], name: "index_contracts_on_cancellation_assignee_id", using: :btree
  add_index "contracts", ["seller_id"], name: "index_contracts_on_seller_id", using: :btree

  create_table "devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "device_token"
    t.string   "device_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "donation_paypal_logs", force: :cascade do |t|
    t.integer  "donation_id"
    t.text     "log"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "donation_paypal_logs", ["donation_id"], name: "index_donation_paypal_logs_on_donation_id", using: :btree

  create_table "donations", force: :cascade do |t|
    t.integer  "user_id",                            null: false
    t.integer  "donatee_id",                         null: false
    t.integer  "amount_cents",                       null: false
    t.integer  "status",                 default: 0, null: false
    t.string   "pay_pal_transaction_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "donations", ["donatee_id"], name: "index_donations_on_donatee_id", using: :btree
  add_index "donations", ["pay_pal_transaction_id"], name: "index_donations_on_pay_pal_transaction_id", using: :btree
  add_index "donations", ["status"], name: "index_donations_on_status", using: :btree
  add_index "donations", ["user_id"], name: "index_donations_on_user_id", using: :btree

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "favorited_user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "favorites", ["favorited_user_id"], name: "index_favorites_on_favorited_user_id", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "game_game_system_joins", force: :cascade do |t|
    t.integer  "game_id"
    t.integer  "game_system_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "game_game_system_user_joins", force: :cascade do |t|
    t.integer  "game_game_system_join_id"
    t.integer  "user_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "game_systems", force: :cascade do |t|
    t.string   "title"
    t.string   "abbreviation"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "games", force: :cascade do |t|
    t.string   "title"
    t.string   "game_cover"
    t.string   "game_jumbo"
    t.string   "game_logo"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "game_jumbo_mobile"
    t.datetime "release_date"
  end

  create_table "invites", force: :cascade do |t|
    t.integer  "contract_id"
    t.integer  "user_id"
    t.integer  "status",      default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "position"
  end

  add_index "invites", ["contract_id", "user_id"], name: "index_invites_on_contract_id_and_user_id", unique: true, using: :btree
  add_index "invites", ["contract_id"], name: "index_invites_on_contract_id", using: :btree
  add_index "invites", ["user_id"], name: "index_invites_on_user_id", using: :btree

  create_table "links", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "clan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",               default: ""
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "conversationable_id"
    t.string   "conversationable_type"
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "notices", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "admin_id"
    t.integer  "clan_id"
    t.string   "type"
    t.string   "title"
    t.text     "body"
    t.datetime "expiration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "notices", ["clan_id"], name: "index_notices_on_clan_id", using: :btree

  create_table "ratings", force: :cascade do |t|
    t.integer  "user_id",                         null: false
    t.integer  "rated_user_id",                   null: false
    t.integer  "personality_rating", default: 0
    t.integer  "approval_rating",    default: 0
    t.integer  "skill_rating",       default: 0
    t.text     "comment",            default: ""
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "ratings", ["rated_user_id"], name: "index_ratings_on_rated_user_id", using: :btree
  add_index "ratings", ["user_id", "rated_user_id"], name: "index_ratings_on_user_id_and_rated_user_id", unique: true, using: :btree
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "reportable_id"
    t.string   "reportable_type"
    t.integer  "user_id"
    t.text     "comment"
    t.integer  "admin_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.text     "admin_comment"
    t.datetime "handled_at"
  end

  add_index "reports", ["admin_id"], name: "index_reports_on_admin_id", using: :btree
  add_index "reports", ["user_id"], name: "index_reports_on_user_id", using: :btree

  create_table "roster_messages", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "roster_id"
  end

  create_table "subscription_plans", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price"
    t.boolean  "recurring"
    t.integer  "period"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "product_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id",                          null: false
    t.integer  "subscription_plan_id",             null: false
    t.integer  "state",                default: 0, null: false
    t.date     "ends_on",                          null: false
    t.string   "token",                            null: false
    t.string   "profile_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "platform",             default: 0, null: false
  end

  add_index "subscriptions", ["subscription_plan_id"], name: "index_subscriptions_on_subscription_plan_id", using: :btree
  add_index "subscriptions", ["user_id"], name: "index_subscriptions_on_user_id", using: :btree

  create_table "system_avatars", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                       default: "",    null: false
    t.string   "encrypted_password",          default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "country"
    t.string   "state"
    t.string   "zipcode"
    t.string   "avatar"
    t.string   "timezone"
    t.string   "city"
    t.string   "username"
    t.text     "bio"
    t.text     "language"
    t.string   "psn_user_name"
    t.string   "xbox_live_user_name"
    t.text     "ground_rules"
    t.string   "newbie_patience_level"
    t.string   "will_play"
    t.string   "may_record_or_stream"
    t.string   "playing_games_since"
    t.string   "playing_for_charity"
    t.string   "paypal_email"
    t.integer  "charity_id"
    t.integer  "personality_rating",          default: 0
    t.integer  "approval_rating",             default: 0
    t.integer  "skill_rating",                default: 0
    t.integer  "psa_rating",                  default: 0
    t.integer  "required_personality_rating", default: 0
    t.integer  "required_approval_rating",    default: 0
    t.integer  "required_skill_rating",       default: 0
    t.integer  "required_psa_rating",         default: 0
    t.integer  "required_cancellation_rate"
    t.integer  "contracts_completed",         default: 0
    t.date     "date_of_birth"
    t.integer  "system_avatar_id"
    t.string   "twitch_video_url"
    t.string   "youtube_video_url"
    t.date     "trial_expiration"
    t.string   "pc_user_name"
    t.string   "nintendo_user_name"
    t.datetime "read_announcement_at"
    t.integer  "generosity_rating",           default: 0
    t.boolean  "public_age"
    t.string   "authentication_token"
    t.boolean  "deleted_account",             default: false
    t.integer  "cancellation_rate"
    t.boolean  "custom_avatar"
    t.boolean  "notif_system",                default: true
    t.boolean  "notif_push",                  default: true
    t.boolean  "notif_sms",                   default: true
    t.boolean  "notif_games",                 default: true
    t.boolean  "notif_email",                 default: true
    t.boolean  "notif_reminder",              default: true
    t.boolean  "show_on_playerlist",          default: true
    t.boolean  "notif_site",                  default: true,  null: false
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["contracts_completed"], name: "index_users_on_contracts_completed", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["system_avatar_id"], name: "index_users_on_system_avatar_id", using: :btree

  create_table "video_urls", force: :cascade do |t|
    t.string   "url"
    t.integer  "clan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "blocks", "contracts"
  add_foreign_key "blocks", "users"
  add_foreign_key "blocks", "users", column: "blocked_user_id"
  add_foreign_key "clan_invites", "clans"
  add_foreign_key "clan_invites", "users"
  add_foreign_key "clan_invites", "users", column: "inviter_id"
  add_foreign_key "clan_members", "clan_ranks"
  add_foreign_key "clan_ranks", "clans"
  add_foreign_key "clans", "users", column: "host_id"
  add_foreign_key "contact_requests", "users"
  add_foreign_key "contracts", "users", column: "seller_id"
  add_foreign_key "contracts", "users", column: "seller_id"
  add_foreign_key "devices", "users"
  add_foreign_key "donation_paypal_logs", "donations"
  add_foreign_key "donations", "users"
  add_foreign_key "donations", "users", column: "donatee_id"
  add_foreign_key "favorites", "users"
  add_foreign_key "favorites", "users", column: "favorited_user_id"
  add_foreign_key "invites", "contracts"
  add_foreign_key "invites", "users"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "ratings", "users"
  add_foreign_key "ratings", "users", column: "rated_user_id"
  add_foreign_key "reports", "admins"
  add_foreign_key "reports", "users"
  add_foreign_key "subscriptions", "subscription_plans"
  add_foreign_key "subscriptions", "users"
end
