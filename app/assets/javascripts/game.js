function bindGameModal() {
    var _sys = $('#user_game_system_ids');
    checkSys(_sys.val());

    _sys.change(function() {
        checkSys($('#user_game_system_ids').val())
    });

    $('form').submit(function() {
        var errors = false
        if (!$('#ign').prop('disabled') && !$('#ign').val()) {
            $('#ign').attr('placeholder', 'Please enter your IGN');
            $('#ign').css('border', '2px solid red');
            $('#ign').focus();
            errors = true;
        }
        if (!$('#user_game_system_ids').val()) {
            $('#user_game_system_ids').css('border', '2px solid red');
            errors = true;
        }
        return errors ? false : true;
    });
}

function checkSys(id) {
    $.get('/profile/check-user-system', { sys: id }).done(function(data) {
        var ign = $('#ign');
        var container = $('#ign-container');
        if (data) {
            ign.prop('disabled', true);
            container.hide();
        } else {
            ign.prop('disabled', false);
            container.show();
            $('#ign-type').text($('#user_game_system_ids option:selected').text());
            ign.focus();
        }
    });
}
