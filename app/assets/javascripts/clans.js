Clans = {}
Clans.CustomLinks = {
  addFields: function(){
    var count = $('.row.link:visible').length;
    if(count < 4){
      num = new Date().getTime();
      var element = '<div class="row link" id="remove_'+num+'"> <div class="col-md-4"> <div class="form-group"> <label for="clan_links_attributes_'+num+'_name">Name</label> <input type="text" id="clan_links_attributes_'+num+'_name" name="clan[links_attributes]['+num+'][name]" placeholder="Link Name" class="form-control"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label for="clan_links_attributes_'+num+'_url">URL</label> <input type="text" id="clan_links_attributes_'+num+'_url" name="clan[links_attributes]['+num+'][url]" placeholder="URL" class="form-control"> </div> </div> <div class="col-md-4 remove_link" id="rm_btn_'+num+'"> <input class="show_link" type="hidden" value="false" name="clan[links_attributes]['+num+'][_destroy]" id="clan_links_attributes_'+num+'__destroy"> <i class="fa fa-times"></i> </div> </div>';
      $('.custom_links').append(element);
    }
  },
  removeFields: function(element){

    $(element).find( ".show_link" ).val("true");
    $(element).find( ".show_link" ).removeAttr('class');
    $(element).hide();
  },
  bindOnClickOnAddLink: function(){
    $('.link_add_button').on('click', function(){
      Clans.CustomLinks.addFields();
    });
  },
  bindOnClickOnRemoveLink: function(){
    $('body').on('click', '.remove_link', function(){
      Clans.CustomLinks.removeFields($(this).closest('.row.link'));
    })
  },
};

Clans.VideoUrl = {
  addFields: function(){
    var count = $('.row.video:visible').length;
    if(count < 10){
      num = new Date().getTime();
      var element = '<div class="row video"><div class="col-md-4"> <div class="form-group"> <label for="clan_video_urls_attributes_'+num+'_name">Video Name</label>            <input class="form-control" placeholder="Video Name" type="text" value="" name="clan[video_urls_attributes]['+num+'][name]" id="clan_video_urls_attributes_4_name"></div></div> <div class="col-md-7"> <div class="form-group"> <label for="clan_video_urls_attributes_'+num+'_url">Video URL</label> <input class="form-control" placeholder="Video URL" type="text" name="clan[video_urls_attributes]['+num+'][url]" id="clan_video_urls_attributes_'+num+'_url"> </div> </div> <div class="col-md-1"> <div class="remove_video"> <input class="show_video" type="hidden" value="false" name="clan[video_urls_attributes]['+num+'][_destroy]" id="clan_video_urls_attributes_'+num+'__destroy"> <i class="fa fa-times"></i> </div> </div> </div>';

      $('.video_url').append(element);
    }
  },
  removeFields: function(element){
    $(element).find( ".show_video" ).val("true");
    $(element).find( ".show_video" ).removeAttr('class');
    $(element).hide();
  },
  bindOnClickOnAddLink: function(){
    $('.video_add_button').on('click', function(){
      Clans.VideoUrl.addFields();
    });
  },
  bindOnClickOnRemoveLink: function(){
    $('body').on('click', '.remove_video', function(){
      Clans.VideoUrl.removeFields($(this).closest('.row.video'));
    })
  },
}
Clans.Question = {
  addFields: function(){
    new_id = new Date().getTime();
    var element = '<div class="row questions"> <div class="col-md-11"> <div class="form-group"> <label for="clan_questions_attributes_'+new_id+'_name">Question</label> <input class="form-control" placeholder="Question" type="text" name="clan[questions_attributes]['+new_id+'][name]" id="clan_questions_attributes_'+new_id+'_name"> </div> </div> <div class="col-md-1"> <div class="remove_question"> <input class="show_question" type="hidden" value="false" name="clan[questions_attributes]['+new_id+'][_destroy]" id="clan_questions_attributes_'+new_id+'__destroy"> <i class="fa fa-times"></i></div></div></div>'
    $('.clan_questions').append(element);
  },
  removeFields: function(element){
    $(element).find( ".question_destroy" ).val("true");
    $(element).find( ".question_destroy" ).removeAttr('class');
    $(element).hide();
  },
  bindOnClickOnAddQuestion: function(){
    $('.question_add_button').on('click', function(){
      Clans.Question.addFields();
    });
  },
  bindOnClickOnRemoveQuestion: function(){
    $('body').on('click', '.remove_question', function(){
      Clans.Question.removeFields($(this).closest('.row.questions'));
    })
  }
}

Clans.Validation = {
  setCharLimit: function(element_selector, limit){
    $(element_selector).on('keyup keypress change', function(event){
      if($(this).val().length > 40){
        event.preventDefault();
      }else{

      }
    });
  },
  validateMotto: function(){
    this.setCharLimit('#clan_motto', 40);
  }
}

Clans.AnnualDues = {
  bindChangeOnAnnualDues: function(){
    $('#clan_annual_dues').on('change', function(event){
        if($(this).val() == '0')
        {
          $('.annual_dues_amount').addClass('hide');
        }
        else{
          $('.annual_dues_amount').removeClass('hide');
        }
    })
  }
}
Clans.MemberRank = {
  bindChangeOnClanRank: function(){
    $('.clan_member_rank_select').on('change', function(event){
      var  clan_member = $(this).data('id');
      var clan = $(this).data('clan')
      var  clan_rank = $(this).val();
      $.ajax({
        method: "PATCH",
        url: "/clans/"+clan+"/clan_members/"+clan_member,
        data: { clan_member: {clan_rank_id: clan_rank} }
      })

    })
  }
}

Clans.MemberUnblock = {
  bindOnClickOnUnblock: function(){
    $('.clan_member_unblock').on('click', function(event){
      clan = $(this).data('clan');
      clan_member = $(this).data('id');
      $.ajax({
        method: "PATCH",
        url: "/clans/"+clan+"/unblock_member/"+clan_member
      })
      $(this).parent().parent().remove();
    })
  }
}

Clans.MemberRemove = {
  bindOnClickRemove: function(){
    $('.clan_member_remove').on('click', function(event){
      clan = $(this).data('clan');
      clan_member = $(this).data('id');
      $.ajax({
        method: "DELETE",
        url: "/clans/"+clan+"/remove_member/"+clan_member
      })
      $(this).parent().parent().remove();
    })
  }
}

$(document).ready(function(){
  //For Link Url
  Clans.CustomLinks.bindOnClickOnAddLink();
  Clans.CustomLinks.bindOnClickOnRemoveLink();

  //For video Url
  Clans.VideoUrl.bindOnClickOnAddLink();
  Clans.VideoUrl.bindOnClickOnRemoveLink();

  // For Question
  Clans.Question.bindOnClickOnAddQuestion();
  Clans.Question.bindOnClickOnRemoveQuestion();

  //For Motto Validation
  Clans.Validation.validateMotto();

  //For AnnualDues
  Clans.AnnualDues.bindChangeOnAnnualDues();

  //Fro Clan members
  Clans.MemberRank.bindChangeOnClanRank();
  Clans.MemberUnblock.bindOnClickOnUnblock();
  Clans.MemberRemove.bindOnClickRemove();

});
