(function($) {
    $(function() {
        if($('#sign_up').length == 0) return;

        var stepsVisited = [];

        $('#sign_up').steps({
            headerTag: 'h2',
            enableAllSteps: true,
            labels: {
                finish: 'Sign Up'
            },

            onInit: function() {
                var $allButtons = $(this).find('.actions ul li'),
                    $submit = $allButtons.last();

                $allButtons.addClass('btn').addClass('btn-x');
                $submit.addClass('btn-primary').addClass('disabled');
                $allButtons.click(function(e) {
                    e.stopPropagation();
                    $(this).find('a').first().click();
                });

                $('#new_user #sign_up-p-0 select').chosen();
                stepsVisited.push(0);
            },

            onFinishing: function() {
                var termsAgreed = $('#new_user #termsAgreed').prop('checked');
                if (!termsAgreed) return false;

                $('#new_user').submit();
            },

            onStepChanged: function(e, ix) {
                if (stepsVisited.indexOf(ix) !== -1) return;
                $('#new_user #sign_up-p-' + ix + ' select').chosen();
                stepsVisited.push(ix);
            }
        });



		$('#termsAgreed').on('change', function() {
            var $btn = $('#sign_up div.actions ul li').last();
			if ($(this).is(":checked")) {
				$btn.removeClass('disabled');
			}
			else {
				$btn.addClass('disabled');
			}
		});

		// Check the toggled system options and show the field
		$('.console-checks input[type="checkbox"]').on('change', function() {
			var system = $(this).attr('value');
			var sysUser = system.substring(0, 2);

			if ($(this).is(":checked")) {
				$('.boolean-console-user[data-console=' + sysUser + ']').slideDown();
				$('.boolean-console-games[data-console=' + system + ']').slideDown();
			}
			else {
				if ($('.console-checks input[type="checkbox"][value^=' + sysUser + ']:checked').length == 0) {
					$('.boolean-console-user[data-console=' + sysUser + ']').slideUp();
					$('.boolean-console-user[data-console=' + sysUser + '] input').val('');
					$('.boolean-console-games[data-console=' + system + ']').slideUp();
					$('.boolean-console-user[data-console=' + sysUser + '] input').val('');
				}
			}
		});

		// Check if a field has data in it and make sure we show it
		// after a failed validation
		$('.boolean-console-games input').each(function() {
			if ($(this).val().length > 0) {
				var system = $(this).parent().parent().attr('data-console');
				$('input[value^="' + system + '"]').attr('checked','checked');
			}
		});

		$('.console-checks input[type="checkbox"]').each(function(){
			var system = $(this).attr('value');
			var sysUser = system.substring(0, 2);

			if ($(this).is(":checked")) {
				$('.boolean-console-user[data-console=' + sysUser + ']').show();
				$('.boolean-console-games[data-console=' + system + ']').show();
			}
			else {
				if ($('.console-checks input[type="checkbox"][value^=' + sysUser + ']:checked').length < 1) {
					$('.boolean-console-user[data-console=' + sysUser + ']').hide();
					$('.boolean-console-user[data-console=' + sysUser + '] input').val('');
				}
				$('.boolean-console-games[data-console=' + system + ']').hide();
				$('.boolean-console-user[data-console=' + sysUser + '] input').val('');
			}
		});
    });
})(jQuery);
