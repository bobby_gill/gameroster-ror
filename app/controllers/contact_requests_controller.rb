class ContactRequestsController < InheritedResources::Base
  respond_to :html

  def create
    # normal routes to make this happen were just *not* working out
    @contact_request = ContactRequest.new(permitted_params[:contact_request])
    if current_user.present?
      @contact_request.email = current_user.email
      @contact_request.user = current_user
    end
    create!{
      if @contact_request.persisted?
        render 'contact_requests/thanks'
        return
      end
    }
  end

  protected
    def permitted_params
		    params.permit(contact_request: [:name, :email, :contact_type, :message])
    end
end
