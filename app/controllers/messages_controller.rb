class MessagesController < ApplicationController
	include MessagesHelper
	before_action :authenticate_user!

	def index
		@box = current_user.mailbox.conversations(:mailbox_type => 'not_trash')
		@box_type = 'Inbox'
	end

	def notifications
		@box = current_user.mailbox.notifications.limit(100)
		@box_type = "Notifications"
		@box.each do |notification|
			begin
				if notification.notified_object_type == "ClanInvite" 
					ClanInvite.find(notification.notified_object_id)
				end
			rescue
			 	notification.destroy
			end
		end
		@box = current_user.mailbox.notifications.limit(25)
		render 'messages/index'
	end

	def trash
		@box = current_user.mailbox.trash
		@box_type = 'Trash'
		render 'index'
	end

	def settings
		@box_type = 'Settings'
	end

	def show
		@conversation = current_user.mailbox.conversations.find(params[:id])
		@box_type = @conversation.is_completely_trashed?(current_user) ? 'trash' : 'messages'
		@conversation.mark_as_read(current_user)

		# figure out who this is with
		@other_user = current_user
		@conversation.participants.each do |p|
			if p != current_user
				@other_user = p
			end
		end

		@origin = @conversation.originator

		if @origin != current_user
			@other_participants = @conversation.participants.uniq{|x| x.id}.delete_if {|obj| obj == @origin || obj == @current_user}
		else
			if @origin != @conversation.participants.uniq{|x| x.id}.first
				@first_recipient = @conversation.participants.first
				@other_participants = @conversation.participants.uniq{|x| x.id}.delete_if {|obj| obj == @origin }
			else
				@first_recipient = @conversation.participants[1]
				@other_participants = @conversation.participants.uniq{|x| x.id}.drop(1)
			end
		end

	end

	def new
		recipient = User.find params[:recipient_id]
		if recipient.is_blocking_user?(current_user) or current_user.is_blocking_user?(recipient)
			redirect_to profile_path
		end
	end

	def destroy
		destroy_message!
		redirect_to messages_path
	end


	def create
		if !can_communicate_with_recipient?
			redirect_to profile_path
			return
		end

		send_message!

		if (@receipt.errors.blank?)
			@conversation = @receipt.conversation if !@conversation.present?
			flash[:success]= t('mailboxer.sent')
			redirect_to message_path(@conversation)
		else
			render message_path(@conversation)
		end
	end

	protected
	def can_communicate_with_recipient?
		# figure out who this is with
		if params[:recipient_id].present?
			if params[:recipient_id].class == Array
				recipients = User.where :id => params[:recipient_id]
			else
				recipients = [User.find(params[:recipient_id])]
			end
		else
			conversation = current_user.mailbox.conversations.find(params[:conversation_id])
			recipients = []
			conversation.participants.each do |p|
				if p != current_user
					recipients << p
				end
			end
		end

		can_communicate = true
		recipients.each do | r |
			can_communicate = false if r.is_blocking_user?(current_user) or current_user.is_blocking_user?(r)
		end

		can_communicate
	end

	# Sends the message using the parameters on the request. Also sets `@conversation` and `@receipt`
	def send_message!
		if params[:conversation_id].present?
			@conversation = current_user.mailbox.conversations.find(params[:conversation_id])
		else
			@conversation = nil
		end
		if @conversation.present?
			@receipt = current_user.reply_to_conversation(@conversation, params[:message], "Reply From #{current_user.username}")
			begin
				NotificationWorker.perform_async(@receipt.message.id, 'Mailboxer::Message', 'Notifications::MessageNotification', 'conversation_reply') if @receipt.errors.blank?
			rescue => e
				puts e
			end
		else
			if params[:recipient_id].present?
				@recipient = User.find(params[:recipient_id])
			else
				#dunno why you'd wanna do this, but ok.
				@recipient = current_user
			end

			@receipt = current_user.send_message( (@recipient.class == Array ? @recipient : [@recipient]) , params[:message], "Message From #{current_user.username}")
			begin
				NotificationWorker.perform_async(@receipt.message.id, 'Mailboxer::Message', 'Notifications::MessageNotification', 'new_conversation') if @receipt.errors.blank?
			rescue => e
				puts e
			end
		end
	end

	# Trashes the conversation using the parameters passed in the request.
	def destroy_message!
			@conversation = current_user.mailbox.conversations.find(params[:id])

			if @conversation.present?
				if @conversation.is_trashed?(current_user)
					@conversation.untrash(current_user)
				else
					@conversation.move_to_trash(current_user)
				end

			end
	end
end
