class Profile::BlocksController < InheritedResources::Base
	defaults :resource_class => Block
	before_filter :authenticate_user!
	respond_to :html, :js

  def new
    @contract = Contract.find params[:contract_id] if params[:contract_id].present?
    @blocked_user = User.find params[:blocked_user_id]

    # sanity check
    is_error = !is_valid_block(@contract, @blocked_user)

    if is_error
      render text: ''
      return
    end

    @block = Block.new
    @block.blocked_user = @blocked_user
    @block.contract = @contract if @contract.present?
    super
  end

	def create
    @contract = Contract.find params[:block][:contract_id] if params[:contract_id].present?
    @blocked_user = User.find params[:block][:blocked_user_id]
    # sanity check
    is_error = !is_valid_block(@contract, @blocked_user)

    if is_error
      render text: ''
      return
    end

		create!(:notice => "#{@blocked_user.username} has been blocked"){ redirect_path }
	end

	def destroy
		username = resource.blocked_user.username
    destroy!(:notice => "#{username} has been unblocked"){
			path = redirect_path
			if is_mobile_app? && path.index('v=blocks').nil?
				path += '?v=blocks'
			end
			path
		}
	end

	private
    def permitted_params
  		params.permit(:blocked_user_id, :contract_id, block: [:blocked_user_id, :contract_id])
    end

	protected
    def begin_of_association_chain
      current_user
    end

    def is_valid_block(contract, blocked_user)
      if contract.present? && contract.buyer == current_user
        contract.seller == blocked_user
      elsif contract.present? && contract.seller == current_user
        contract.buyer == blocked_user
      else
        current_user != blocked_user
      end
    end

    def redirect_path
			request.referer
    end

    def blocks_and_feedback_path
      profiles_path + '/' + current_user.username + '/blocks-and-feedback'
    end

    def feedback_path
      profiles_path + '/' + current_user.username + '/feedback'
    end
end
