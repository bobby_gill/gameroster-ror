class Profile::GamesAndSystemsController < InheritedResources::Base
  defaults :resource_class => User
  before_filter :authenticate_user!
  respond_to :html


  def update
    resource.validate_game = true
    update!{edit_profile_games_and_systems_path}
  end

  def create
    redirect_to edit_profile_games_and_systems_path
  end

  def destroy
    redirect_to edit_profile_games_and_systems_path
  end

  def add_game
    @errors = []
    @game = params[:game][:id]
    @game_name = Game.find(@game).title
    @system = params[:user][:game_system_ids]
    @system_name = GameSystem.find(@system).title
    @ign = params[:system].present? ? params[:system][:ign] : nil

    @game_system_join = GameGameSystemJoin.where(game_id: @game).where(game_system_id: @system).first
    @errors << 'Invalid system for this game' unless @game_system_join

    if GameGameSystemUserJoin.where(game_game_system_join_id: @game_system_join.id)
        .where(user_id: current_user.id).empty?

      unless GameGameSystemUserJoin.create(game_game_system_join_id: @game_system_join.id, user_id: current_user.id)
        @errors << 'An error has occurred. Please try again later.'
      end

    else
      @errors << "#{ @game_name } is already in your profile for #{@system_name}."
    end

    # check ign & add if new ign is present.
    if @ign
      case @system_name
      when 'Xbox One', 'Xbox 360'
        current_user.xbox_live_user_name = @ign
      when 'PlayStation 4', 'PlayStation 3'
        current_user.psn_user_name = @ign
      when 'PC'
        current_user.pc_user_name = @ign
      when 'Nintendo Wii U'
        current_user.nintendo_user_name = @ign
      else
        @errors << "#{ @system_name } is invalid."
      end
      @errors << "Could not save IGN" unless current_user.save
    end

    if @errors.count > 0
      flash[:error] = @errors.join('\n')
    else
      flash[:success] = "#{ @game_name } has been added to your profile on #{ @system_name }."
    end

    redirect_to dashboard_path
  end

  def check_game
    sys = params[:sys]
    res = current_user.game_systems.find_by(id: sys)
    render json: res
  end

  private
  def permitted_params
    params.permit(:user => [:psn_user_name, :xbox_live_user_name, :game_game_system_join_ids => []])
  end

  protected
  def resource
    current_user
  end

end
