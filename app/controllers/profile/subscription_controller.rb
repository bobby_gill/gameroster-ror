class Profile::SubscriptionController < InheritedResources::Base
	defaults :resource_class => User
	before_filter :authenticate_user!
	respond_to :html

  def index
    @subscription_plans = SubscriptionPlan.order(price: :asc).all
  end

	protected
    def resource
      current_user
    end

end
