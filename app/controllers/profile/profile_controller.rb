class Profile::ProfileController < InheritedResources::Base
	defaults :resource_class => User
	before_filter :authenticate_user!
	respond_to :html

	def show
		@grid = initialize_grid(user_contracts,
			order: 'start_date_time',
			order_direction: 'desc',
			per_page: 30,
			name: 'grid',
		                       )
                @twitch_stream = TwitchAPI::user_stream(current_user)
                @ign_list = []
                # make a little easier to work with than ""
                @psn = resource.psn_user_name.empty? ? nil : [resource.psn_user_name, 'PlayStation Network']
                @xbox = resource.xbox_live_user_name.empty? ? nil : [resource.xbox_live_user_name, 'Xbox Live']
                @pc = resource.pc_user_name.empty? ? nil : [resource.pc_user_name, 'PC']
                [@psn, @xbox, @pc].each{ |i| @ign_list << i if i }

	end

	def edit
		@site_averages = site_averages
		super
	end

	def update
		resource.do_profile_valdation = true
		@site_averages = site_averages
		update!{edit_profile_path}
	end

	def create
		redirect_to edit_profile_path
	end

	def destroy
		redirect_to edit_profile_path
	end

	private
    def permitted_params
		params.permit(user: [:bio, :avatar, :custom_avatar, :timezone,
			:language, :ground_rules, :psn_user_name, :xbox_live_user_name, :pc_user_name, :nintendo_user_name, :will_play, :newbie_patience_level,
			:may_record_or_stream, :playing_games_since, :playing_for_charity,
			:charity, :charity_id, :required_personality_rating,
			:required_approval_rating, :required_skill_rating,
			:required_cancellation_rate, :date_of_birth, :system_avatar_id,
			:twitch_video_url, :youtube_video_url, :languages => []
		])
    end

	protected
    def resource
      current_user
    end
	def site_averages
		{
			personality_rating: User.average(:personality_rating).floor,
			approval_rating: User.average(:approval_rating).floor,
			skill_rating: User.average(:skill_rating).floor,
			cancellation_rate: ((Contract.cancelled.count.to_f / [Contract.closed.count.to_i, 1].max) * 100).round
		}
	end
	def user_contracts
		user = current_user
		Contract.where('((contract_type = ? AND seller_id = ?) OR (contract_type = ? AND buyer_id = ?)) AND status = ? ', 'Contract', user.id, 'Roster', user.id, 'Open')
	end
end
