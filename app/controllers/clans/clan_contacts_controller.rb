class Clans::ClanContactsController < ApplicationController
  before_action :authenticate_user!
  respond_to :html, :js

  def create
    clan = Clan.find(params[:clan_id])
    clan.clan_members.each do |clan_member|
      if clan_member.clan_rank.present?
        current_user.send_message(clan_member.user, params[:message], "Message From #{current_user.username}") if clan_member.clan_rank.receive_contact?
      end
    end
    redirect_to clan
  end
end
