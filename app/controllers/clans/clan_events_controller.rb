class Clans::ClanEventsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html, :js

	def index
		@clan = Clan.find(params[:clan_id])
		#@events = @clan.get_contracts.where('start_date_time > now()').order("start_date_time ASC").page(params[:page]).per(params[:per_page])
    @events = Contract.all.where('date(start_date_time) > now()').order("start_date_time ASC").page(params[:page])
    respond_to do |format|
      format.html
      format.js {render layout: false}
    end
	end

  private

end