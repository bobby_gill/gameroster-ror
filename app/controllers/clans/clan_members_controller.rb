class Clans::ClanMembersController < ApplicationController
  before_action :authenticate_user!
  respond_to :html, :js

  def create
    @clan = Clan.find(params[:clan_id])
    if !@clan.private? && @clan.autojoin?
      @clan.add_member(member_params)
      flash[:info] = "You are now a member of #{@clan.name}"
      redirect_to @clan
    else
      @invite  = ClanInvite.find(params[:invite_id])
      if @invite.approved?
        @clan.add_member(member_params)
        flash[:info] = "You are now a member of #{@clan.name}"
        redirect_to @clan
      else 
        flash[:danger] = "Could not join clan, try again"
        redirect_to @clan
      end
    end
  end

  def index
    @clan = Clan.find(params[:clan_id])
    @clan_members = @clan.clan_members.where.not(user_id: @clan.host.id ).page(params[:page]).per(params[:per_page]) rescue nil
    respond_to do |format|
      format.html
      format.js {render layout: false}
    end
  end

  def edit
    @member = ClanMember.find(params[:id])
    @clan = @member.clan

    respond_to do |format|
      format.html
      format.js {render layout: false}
    end
  end

  def update
    @clan_member = ClanMember.find(params[:id])
    @clan = @clan_member.clan
    if @clan_member.update(member_params)
      cm = @clan_member.as_json
      cm["title"] = @clan_member.clan_rank.title
      respond_to do |format|
        format.html {redirect_to clan_clan_members_path(@clan)}
        format.js {render layout: false}
      end
    end
  end

  def destroy
    @member = ClanMember.find(params[:id])
    @clan = @member.clan
    if current_user == @member.clan.host || current_user == @member.user
      ClanMember.find(params[:id]).destroy
    end
    redirect_to @clan
  end

  private
    def member_params
      params.require(:clan_member).permit(:user_id, :clan_id, :clan_rank_id)
    end

end