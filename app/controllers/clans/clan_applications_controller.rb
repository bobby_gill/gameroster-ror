class Clans::ClanApplicationsController < ApplicationController

  before_action :authenticate_user!
  before_action :clan
  before_filter :is_application_reviewer!, except: [:create]
  before_filter :clan_application, only: [:accept, :destroy]
  respond_to :html, :js

  def index
    @clan_applications = @clan.clan_applications.includes(:user => :system_avatar, :answers => [:question])
    respond_to do |format|
      format.html { redirect_to clan_path @clan}
      format.js {render layout: false}
    end
  end

  def create
    clan_application = ClanApplication.new(clan_application_params)
    if clan_application.save
      redirect_to clan_path(clan_application.clan)
      flash[:notice] = "Application to join clan '#{@clan.name}' has been sent!"
      clan_application.send_message
    else
      flash[:success] = "Application is not Posted Please try again"
      redirect_to clan_path(clan_application.clan)
    end
  end

  def show
    @clan_application = @clan.clan_applications.with_deleted.includes(:user, :reviewer ,:answers => [:question]).find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def accept
    @clan.clan_members.create(user_id: @clan_application.user_id)
    @clan_application.update_attributes(reviewer_id: current_user.id, reviewed_at: Time.now, status: true)
    @clan_application.destroy
    flash[:success] = "Application Accepted"
    redirect_to clan_clan_application_path(@clan, @clan_application)
  end

  def destroy
    @clan_application.update_attributes(reviewer_id: current_user.id, reviewed_at: Time.now, status: false)
    @clan_application.destroy
    flash[:success] = "Application Rejected"
    redirect_to clan_clan_application_path(@clan, @clan_application)
  end


  private

  def clan_application_params
    params.require(:clan_application).permit(:clan_id, :user_id,
    answers_attributes: [:id, :question_id, :user_id, :clan_id, :answer])
  end

  def clan
    @clan = Clan.find(params[:clan_id])
  end

  def clan_application
    @clan_application = @clan.clan_applications.find(params[:id])
  end

  def is_application_reviewer!
    clan_member = @clan.clan_members.find_by_user_id(current_user.id)
    if @clan.is_host?(current_user)
    elsif clan_member.present? && clan_member.clan_rank.review_applications?
    else
      flash[:error] = "You are not Application reviewer"
      redirect_to clan
    end
  end
end
