class Clans::ClansController < ApplicationController
	include SortsAndFilters
	before_action :clan, except: [:new, :create, :index]
	before_filter :authenticate_user!, :except =>[:index, :show]
	before_filter :is_owner, only: [:update, :destroy, :edit, :change_owner, :clan_members_update, :unblock_member]
	before_filter :get_collection, only: [:index]
	before_filter :filter_params, only: [:index]
	before_filter :set_search, only: [:index]
	before_filter :get_clan_form_data, only: [:edit]
  # before_filter :is_premium?, only: :new

	respond_to :html

	def show
		if @clan.present?
			# if not resource.is_member? current_user
			invites = ClanInvite.where(user: current_user).where(status: "pending")
			@clan_invite = nil
			invites.each do |i|
				@clan_invite = i
				if not @clan_invite.is_request? then break end
			end

			# end
			#TODO: Calculate the clan's most active user somehow?
			@most_active = User.where(:id => @clan.most_active).first

	    @clan = clan
			if clan.recruiting? && current_user.present?
				@clan_member = clan.active_member? current_user
			 	@pending_application = current_user.clan_applications.find_by_clan_id(clan.id)
			  @deleted_application = current_user.clan_applications.deleted.find_by_clan_id(clan.id)
			end
	    @clan_notices = clan.clan_notices.current
	    @twitch_stream = TwitchAPI.user_stream @clan
			@clan_donation = @clan.clan_donations.new
		else
			redirect_to clans_path
		end
	end

	def index
		@timezones = ActiveSupport::TimeZone.all.map{|tz| tz.name}
		@games = GameGameSystemJoin.all

		@sorted_games = Game.all.sort_by &:title
		@sorted_systems = GameSystem.all.sort_by &:title

    if params[:my_clans] && eval(params[:my_clans])
      @clans = @resource.joins(:clan_members).where('clan_members.user_id': current_user.id).page params[:page]
    else
      @clans = @resource.page params[:page]
    end
	end

  def new
  	@timezones = ActiveSupport::TimeZone.all.map{|tz| tz.name}
    @sorted_systems = GameSystem.all
    @clan = Clan.new
		3.times {|i| @clan.clan_ranks.build}
		@clan.links.build
		@clan.video_urls.build
    @games = Game.all
  end

  def create
		@timezones = ActiveSupport::TimeZone.all.map{|tz| tz.name}
		@sorted_systems = GameSystem.all
		@clan = Clan.new(clan_params)
    @clan.assign_attributes(host_id: current_user.id)

    # if @clan.save
    #   redirect_to clan_path(@clan)
    # else
    #   flash[:danger] = "Failed to create clan"
    #   redirect_to root_url
    # end

    respond_to do |format|
      if @clan.save
        format.html { redirect_to @clan, notice: 'Clan was successfully created.' }
        format.json { render json: @clan, status: :created, location: @clan }
      else
        @games = Game.all
        format.html { render action: 'new' }
        format.json { render json: @clan.errors, status: :unprocessable_entity }
      end
    end

  end

  def edit

		respond_to do |format|
      format.html
      format.js {render layout: false}
    end
  end

  def update
    if @clan.update(clan_params)
      flash[:success] = "Clan updated"

		else
			flash[:success] = "Clan has some error"
    end
		get_clan_form_data
		render action: :edit
  end

	def change_owner
		if @clan.update(clan_host_params)
			flash[:success] = "Clan Owner changed"
			redirect_to clan_path(@clan)
		else
			flash[:info] = "Clan Owner Not changed"
			redirect_to edit_clan_path(@clan)
		end
	end

  def destroy
    if @clan.destroy
      flash[:info] = "Clan destroyed"
      redirect_to clans_path
    end

  end

  def join
    @clan = resource
    @clan.join current_user
  end

	def remove_member
		clan_member = @clan.clan_members.find(params[:clan_member_id])
		clan_member.destroy if clan_member.present?
		render nothing: true
	end

	def unblock_member
		clan_member = @clan.clan_members.with_deleted.find(params[:clan_member_id])
		clan_member.restore if clan_member.present?
		render nothing: true
	end

	def clan_members_update
		clan_member = @clan.clan_members.find(params[:clan_member_id])
		clan_member.update(clan_member_params)
		render nothing: true
	end

 	private
    def clan
      @clan = Clan.find_by_id(params[:id]) || Clan.search_by_name(params[:id])
    end

    def is_owner
      unless clan.is_host? current_user
        flash[:error] = "You are not the clan owner"
        redirect_to clan
      end
    end

    def get_collection
			clans = Clan.all
			# filter

			# Presort by what?
		 #@family_friendly = params[:family_friendly]
			#clans = clans.order(created_at: :asc)

			@resource = clans
    end

    def is_premium?
      unless current_user.is_premium?
        flash[:error] = 'You must be a premium member to host clans.'
        redirect_to profile_subscription_path unless current_user.is_premium?
      end
    end

    def clan_params
      params.require(:clan).permit(:name, :game_type, :game_system, :play_style,
        :languages, :timezone, :bio, :minimum_age, :family_friendly, :availability, :ground_rules, :cover, :jumbo,
        :mobile_jumbo, :private, :autojoin, :facebook, :youtube, :twitter, :twitch, :bungie, :discord, :youtube_video_url,
        :jumbo_cache, :mobile_jumbo_cache, :cover_cache, :motto, :requirements, :paypal_email, :remove_inactive_users,
				:country, :status, :google, :instagram, :curse, :patreon, :steam, :reddit, :mlg,
				:discord_invitation, :curse_invitation, :wargaming, :battle, :skype,
				:annual_dues, :annual_dues_amount, :re_apply, :most_active, :most_active_days,
				:languages => [],  :clan_game_systems => [],
				links_attributes: [:id, :name, :url, :_destroy],
				video_urls_attributes: [:id, :name, :url, :_destroy],
				game_ids: [], game_system_ids: [],
				clan_ranks_attributes: [:id, :title, :post_events, :post_chat, :post_notices, :review_applications, :permissions, :is_default, :receive_contact],
				questions_attributes: [:id, :name, :_destroy])
    end

		def clan_host_params
			params.require(:clan).permit(:host_id)
		end

		def clan_member_params
			params.require(:clan_member).permit(:clan_rank_id)
		end

		def get_clan_form_data
			@timezones = ActiveSupport::TimeZone.all.map{|tz| tz.name}
	    @sorted_systems = GameSystem.all
	    @games = Game.all
			@clan_members = @clan.clan_members.preload(:user, :clan_rank).where('user_id != ?', @clan.host_id).order(:created_at).page(params[:page]).per(50) rescue nil
			@blocked_members = @clan.clan_members.deleted.preload(:user,:clan_rank).where('user_id != ?', @clan.host_id).order(:created_at).page(params[:page]).per(50) rescue nil
		end
end
