class User::RegistrationsController < Devise::RegistrationsController
	before_filter :configure_sign_up_params, only: [:create]
	before_filter :configure_account_update_params, only: [:update]

	respond_to :html, :js

	# GET /resource/sign_up
	# def new
	#   super
	# end

	# POST /resource
	def create
	  super do |user|
      user.do_country_validation = true
			user.calculate_trial_period params[:promotional_code]
			user.system_avatar_id = rand(1..SystemAvatar.count)
			user.save
		end
	end

	# GET /resource/edit
	# def edit
	#   super
	# end

	# PUT /resource
	# def update
	#   super
	# end

	# DELETE /resource
	# def destroy
	#   super
	# end

	# GET /resource/cancel
	# Forces the session data which is usually expired after sign
	# in to be expired now. This is useful if the user wants to
	# cancel oauth signing in/up in the middle of the process,
	# removing all OAuth session data.
	# def cancel
	#   super
	# end

	def tosmodal

	end

	protected

	# You can put the params you want to permit in the empty array.
	def configure_sign_up_params
		devise_parameter_sanitizer.for(:sign_up) do |u|
			u.permit( :username, :email, :password, :password_confirmation,
				:first_name, :last_name, :group_name , :address_1, :address_2,
				:city, :state, :zipcode, :country, :avatar, :xbox_live_user_name, :psn_user_name, :nintendo_user_name, :pc_user_name, :language, :timezone,
				:date_of_birth, :age, :public_age, :email_confirmation, :languages => []
			)
		end
	end

	# You can put the params you want to permit in the empty array.
	def configure_account_update_params
		devise_parameter_sanitizer.for(:account_update) do |u|
			u.permit(:username, :email, :password, :password_confirmation,
				:first_name, :last_name, :group_name , :address_1, :address_2,
				:city, :state, :zipcode, :country, :avatar, :xbox_live_user_name, :psn_user_name, :nintendo_user_name, :pc_user_name, :language, :timezone,
				:date_of_birth, :public_age, :languages => []
			)
		end
	end

	# The path used after sign up.
	def after_sign_up_path_for(resource)
		thankyou_path
	end

	# The path used after sign up for inactive accounts.
	def after_inactive_sign_up_path_for(resource)
		thankyou_path
	end
end
