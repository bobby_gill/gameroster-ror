module ApplicationHelper

	def contract_filter_text(filters, type='Contract')
		# note: we intentionally skip Game because it gets pretty obvious with the cover
		text = []

		# username
		text << "Username: #{filters["users.username"]}" if filters["users.username"].present?

		# game system
		if filters["game_systems.id"].present?
			gs = GameSystem.find filters["game_systems.id"]
			gss =  []
			gs.each do |g|
				gss << g.title
			end
			text << "#{'Game System'.pluralize gss.count}: #{gss.join ', '}"
		end

		# will play
		text << "Will Play: #{filters["users.will_play"].join ', '}" if filters["users.will_play"].present? && filters["users.will_play"].size > 0

		# newb patience
		text << "Newb Patience: #{filters["users.newbie_patience_level"].join ', '}" if filters["users.newbie_patience_level"].present? && filters["users.newbie_patience_level"].size > 0

		# contacts completed
		text << "Minimum Contracts Completed: #{filters["users.contracts_completed"][:fr]}" if filters["users.contracts_completed"].present? && filters["users.contracts_completed"][:fr].present? && filters["users.contracts_completed"][:fr].to_i  > 0

		# contract price
		if filters[:price_in_cents].present? && filters[:price_in_cents][:fr].present?
			text << 'Donation: ' + (filters[:price_in_cents][:fr].to_i > 0  ? 'Yes' : 'No')
		end

		# start date
		if filters[:start_date_time].present? && (filters[:start_date_time][:fr].present? || filters[:start_date_time][:to].present?)
			fmt = '%Y/%m/%d'
			t = 'Date Range: '
			if filters[:start_date_time][:fr].present? && filters[:start_date_time][:to].present?
				start = filters[:start_date_time][:fr]
				e = filters[:start_date_time][:to]
				t += "#{start.strftime(fmt)} - #{e.strftime(fmt)}"
			elsif filters[:start_date_time][:fr].present?
				start = filters[:start_date_time][:fr]
				t += "#{start.strftime(fmt)} - Future"
			else
				e = filters[:start_date_time][:to]
				t += "Now - #{e.strftime(fmt)}"
			end
			text << t
		end

		if filters[:duration].present? && filters[:duration][:fr].present? && filters[:duration][:fr].size > 0
			t = 'Duration: '
			durations_a = filters[:duration][:fr].map do |d, v|
				duration = d.split('_')[0].to_i/60
				"#{duration} #{"hour".pluralize duration}"
			end
			t += durations_a.join ', '
			text << t
		end

		text << "Mission: #{filters[:mission]}" if filters[:mission].present?
		text << "Class: #{filters[:player_class]}" if filters[:player_class].present?
		text << "Level: #{filters[:level][:fr]}+" if filters[:level].present? && filters[:level][:fr].present?

		raw "Filtering by: <span class=\"filters\">#{text.join(', ')}</span>" if text.size > 0
	end

	def short_time_ago_in_words(time)
		str = time_ago_in_words time
		str = str.gsub 'hours', 'hr.'
		str = str.gsub 'hour', 'hr.'
		str = str.gsub 'minutes', 'min.'
		str = str.gsub 'minute', 'min.'
		str = str.gsub 'seconds', 'sec.'
		str = str.gsub 'second', 'sec.'
	end

	def keys_present?(hash, *path)
		path.inject(hash) do |location, key|
			location.respond_to?(:keys) ? location[key] : nil
		end
	end
end
