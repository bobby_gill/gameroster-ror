module MessagesHelper

  def parse_message(message, user, email=false)
    raw Shortcode.process(message, {current_user: user, email: email})
  end

  def get_recipient(conversation)
  	n_others = conversation.recipients.reject{|u| u.id == current_user.id }
  	if n_others.present?
       n_others.first
    else
    	current_user
    end
  end

end
