class VideoUrl < ActiveRecord::Base
  belongs_to :clan
  validates :url, presence: true
  validate :validate_url

   def youtube_url?
     url.start_with?(YOUTUBE_PREFIX) || url.start_with?(YOUTUBECOM_PREFIX)
   end

   def twitch_url?
     url.start_with?(TWITCH_PREFIX)
   end

   def video_id
     code = url
     if twitch_url?
       code.delete(TWITCH_PREFIX)[0..8]
     elsif youtube_url?
       code.delete(YOUTUBE_PREFIX).delete(YOUTUBECOM_PREFIX)[0..8]
     end
   end
   private

   TWITCH_PREFIX = 'https://www.twitch.tv/videos/'
   YOUTUBECOM_PREFIX = 'https://www.youtube.com/watch?v='
   YOUTUBE_PREFIX= 'https://youtu.be/'

    def validate_url
      unless url.present? && url.start_with?(TWITCH_PREFIX, YOUTUBE_PREFIX, YOUTUBECOM_PREFIX)
        errors.add(:url, 'Accept only youtube and twitch video url')
      end
    end

end
