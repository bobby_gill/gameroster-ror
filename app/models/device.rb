class Device < ActiveRecord::Base
  belongs_to :user
  validates :device_token, presence: true
  validates :device_type, presence: true
end
