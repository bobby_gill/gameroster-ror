class Clan < ActiveRecord::Base
  CLAN_GAME_LIMIT = 4
  has_one :host,  class_name: "User", primary_key: :host_id, foreign_key: :id
  has_many :clan_members, dependent: :destroy
  has_many :clan_notices
	has_many :users, through: :clan_members
	has_many :clan_messages, dependent: :destroy
  has_one :default_rank, class_name: "ClanRank", primary_key: :default_rank_id, foreign_key: :id
  has_many :clan_ranks, dependent: :destroy
  has_many :rosters, class_name: 'Roster', foreign_key: :clan_id
  has_many :contract_game_game_system_joins, through: :rosters
  has_many :games, through: :contract_game_game_system_joins
  has_many :game_systems, through: :contract_game_game_system_joins
  has_many :links
  has_many :video_urls
  has_many :clan_donations
  has_many :questions
  has_many :clan_applications
  has_many :answers
  has_and_belongs_to_many :games
  has_and_belongs_to_many :game_systems

  # validates_each :clan_games do |clan, attr, value|
  #   user.errors.add attr, "too much things for user" if clan.clan_games.size > Clan::CLAN_GAME_LIMIT
  # end

  accepts_nested_attributes_for :links, :allow_destroy => true
  validates_associated :links
  accepts_nested_attributes_for :video_urls, :allow_destroy => true
  validates_associated :video_urls
  accepts_nested_attributes_for :questions, :allow_destroy => true
  validates_associated :questions
  accepts_nested_attributes_for :clan_ranks
  validates_associated :clan_ranks

  validates :name, presence: true
  validates :name, uniqueness: { :case_sensitive => false }
  validates :motto, presence: true
  validates_length_of :motto, :maximum => 40
  validates :game_type, presence: true
  validates :play_style, presence: true
  validates :most_active, presence: true
  validates :most_active_days, presence: true
  validates :bio, presence: true
  validates :ground_rules, presence: true
  validates :requirements, presence: true
  validates :timezone, presence: true
  validates :languages, presence: true
  validates :minimum_age, presence: true
  validates :paypal_email, presence: true
  validates :country, presence: true
  # custome validation
  validate :games_up_to_four, :clan_ranks_only_three, :validate_questions, :validate_game_systems
  validate :validate_popular_links
  validate :validate_annual_dues_amount#, :presence => true, :if => "annual_dues_amount.blank?"

  after_create :init_clan
	mount_uploader :cover, ClanUploader
	mount_uploader :jumbo, ClanUploader
	mount_uploader :mobile_jumbo, ClanUploader

  VALID_USERNAME_REGEX = /\A[a-zA-Z0-9]+\z/
  validates :facebook, length: {maximum: 30}, format: {with: VALID_USERNAME_REGEX}, allow_blank: true
  validates :twitch, length: {maximum: 30}, format: {with: VALID_USERNAME_REGEX}, allow_blank: true
  validates :youtube, length: {maximum: 30}, format: {with: VALID_USERNAME_REGEX}, allow_blank: true
  validates :twitter, length: {maximum: 30}, format: {with: VALID_USERNAME_REGEX}, allow_blank: true


  before_save :save_annual_dues_amount

  include Searchable
  extend  Searchable

  POPULAR_TIMES = ["All Day", "Mornings", "Daytime", "Afternoons", "Nights"]

  POPULAR_DAYS = ["All Days", "Weekdays", "Weekends"]

  GAME_TYPE =  ["All Types", "Player vs. Player", "Player vs. Enemy"]

  AGE_RANGES = {"13-18": "13 - 18",
                "19-26": "19 - 26",
                "27-35": "27 - 35",
                "36-55": "36 - 55",
                "56+": "56+"
               }

  MEMBER_RANGES = {"0-19": "0 - 19",
                "20-100": "20 - 100",
                "101-500": "101 - 500",
                "Above 500": "Above 500"
               }

  INACTIVE_USER_RANGES = {"Never": " ",
                "30 Days": "30",
                "60 Days": "60",
                "90 Days": "90",
                "120 Days": "120",
                "150 Days": "150",
                "180 Days": "180",
                "210 Days": "210",
                "240 Days": "240",
                "270 Days": "270",
                "300 Days": "300",
                "330 Days": "330",
                "360 Days": "360"
               }
  STATUSES = {
              'Open':       'open',
              'Closed':     'closed',
              'Recruiting': 'recruiting'
  }

  ANNUAL_DUES = {
   'Optional' => 1,
   'Yes' => 2,
   'No' =>  0
  }
  MINIMUMAGE = Array(13..35).unshift('None')
  # def join user, invite=nil
  #   if private? and not invite
  #      return nil
  #   else
  #     ClanMember.create(clan_id: id, user: user)
  #   end
  # end

  def recruiting?
    status == 'recruiting'
  end

  def closed?
    status == 'closed'
  end

  def open?
    status == 'open'
  end

  def self.search_by_name(name)
    where("LOWER(name) ILIKE ?", "%#{name.downcase}%").take
  end


  def top_3_game_game_system_joins
    #there has to be a better way to do this...
    contract_game_game_system_joins
    .group('game_game_system_join_id')
    .select('game_game_system_join_id,  count(game_game_system_join_id) as game_count')
    .order('game_count desc').limit(3)

  end

  def top_3_systems
    top_3_game_game_system_joins.map(&:game_system).uniq

  end

   def top_3_games
    top_3_game_game_system_joins.map(&:game).uniq
   end

   def most_popular_game
    top_3_game_game_system_joins.first.game
   end


  def member user
    cm = clan_members.find_by(user_id: user.id)
    return cm ? cm : false
  end

  def member_or_removed_member? user
    cm = clan_members.with_deleted.find_by(user_id: user.id)
    return cm ? cm : false
  end

  def member_removed? user
    cm = clan_members.deleted.find_by(user_id: user.id)
    return cm ? cm : false
  end

  def active_member? user
    cm = clan_members.find_by(user_id: user.id)
    return cm ? cm : false
  end

  def application_reveiewer? user
    member = active_member? user
    if is_host? user
      true
    elsif member
      member.try(:clan_rank).try(:review_applications?)
    else
      false
    end
  end

  def allow_reapply_application?
    re_apply
  end


  def is_host? user
    host_id == user.id
  end

  def can_perform? user, action
    cm = member user
    cm ? cm.can_perform?(action) : false
  end

  # Currently sorts by most recently active games for clan.
  def self.get_clan_titles_for_user user
    game_joins = []
    game_ids = []
    clans = user.clan_members.ids

    # If the user doesn't have any clans show them the most recent contract games
    # that have a clan id associated with it.
    log = clans.empty? ? lambda { |c| c.clan_id != nil } : lambda { |c| clans.include?(c.clan_id) }
    game_joins << Contract.order('start_date_time desc').select{ |c| log.call(c) }

    game_joins[0].each{ |g| game_ids << g.game_game_system_joins.first.game_id }
    return game_ids.uniq.map{ |g| Game.find(g) }
  end

  # return all contracts for a clan newest first
  def get_contracts
    Contract.where(clan_id: id).order('start_date_time desc')
  end

  def get_contracts_upcoming
    Contract.where(clan_id: id).where('start_date_time > ?', Time.now).order('start_date_time desc')
  end

  def get_contracts_previous
    Contract.where(clan_id: id).where('start_date_time < ?', Time.now).order('start_date_time desc')
  end

  def get_game_ids
    contracts = get_contracts
    return contracts.map{ |c| c.game_game_system_joins.first.game_id }.uniq
  end

  def get_games
    games = get_game_ids
    return games.map{ |t| Game.find(t) }
  end

  def get_member_ids
    return clan_members.pluck(:user_id)
  end

  def get_members
    members = get_member_ids
    return members.map{ |m| User.find(m) }
  end

  def get_host
    host_id.nil? ? nil : User.find(host_id)
  end

  def add_member member_params
    ClanMember.create(member_params)
  end

  def add_rank title, permissions, level
    ClanRank.create(clan_id: self.id, title: title, permissions: permissions.map{|p| p.to_sym}, level: level)
  end

  def remove_rank id
    rank = ClanRank.find(id)
    rid = rank.id
    return false if clan_ranks.count < 2 || rank.clan != self || rank == default_rank
    begin
      return !!ClanRank.find(id).destroy
    rescue
      return false
    end
  end

  def init_ranks
    member_rank = add_rank "Member", [:post_to_forum, :post_to_events], 1
    add_rank "Lieutenant", [:invite_users, :accept_users], 2
    set_default_rank member_rank
  end

  def set_default_rank rank
    return false if rank.clan != self
    self.update(default_rank_id: rank.id)
  end




  private
    def init_clan
      ClanMember.create(clan: self, user_id: host_id)
      #init_ranks
    end

    def save_annual_dues_amount
      if annual_dues == 0
        self.annual_dues_amount = 0
      end
    end


    def games_up_to_four
      errors.add(:game_ids, "Select Up to Four Games") if games.size > 4
    end

    def clan_ranks_only_three
      errors.add(:clan_ranks, "Require three Rank") if clan_ranks.size != 3
    end

    def validate_questions
      if status == 'recruiting' && questions.size == 0
        errors.add(:questions, "Require At least one question")
      end
    end

    def validate_annual_dues_amount
      if self.annual_dues == 2 && self.annual_dues_amount == nil
        errors.add(:annual_dues_amount, "can't be blank")
      end
    end

    def validate_game_systems
      errors.add(:game_system_ids, "can't be blank") if game_systems.size == 0
    end

    def validate_popular_links
      if discord_invitation.present? && discord_invitation.strip.include?('discord.gg')
      elsif discord_invitation.present?
        errors.add(:discord_invitation, "Enter valid URL")
      end

      if discord.present? && discord.strip.include?('discordapp.com')
      elsif discord.present?
        errors.add(:discord, "Enter valid URL")
      end

      if curse_invitation.present? && curse_invitation.strip.include?('curse.com')
      elsif curse_invitation.present?
        errors.add(:curse_invitation, "Enter valid URL")
      end

      if curse.present? && curse.strip.include?('curse.com')
      elsif curse.present?
        errors.add(:curse, "Enter valid URL")
      end

      if patreon.present? && patreon.strip.include?('patreon.com')
      elsif patreon.present?
        errors.add(:patreon, "Enter valid URL")
      end

      if steam.present? && steam.strip.include?('steamcommunity.com')
      elsif steam.present?
        errors.add(:steam, "Enter valid URL")
      end

      if mlg.present? && mlg.strip.include?('majorleaguegaming.com')
      elsif mlg.present?
        errors.add(:mlg, "Enter valid URL")
      end

      if wargaming.present? && wargaming.strip.include?('wargaming.net')
      elsif wargaming.present?
        errors.add(:wargaming, "Enter valid URL")
      end

      if battle.present? && battle.strip.include?('battle.net')
      elsif battle.present?
        errors.add(:battle, 'Enter valid URL')
      end
    end

end
