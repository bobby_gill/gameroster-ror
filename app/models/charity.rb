class Charity < ActiveRecord::Base

	has_many :users
	
	mount_uploader :charity_logo, GenericUploader
	
	validates :charity_name, :charity_about, :charity_url, presence: true
	#validates :charity_logo, presence: true

end
