class Question < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :clan
  has_many :answers
  validates :name, presence: true
  validates :clan_id, presence: true
end
