class ClanMember < ActiveRecord::Base
	#soft delete
	acts_as_paranoid

	belongs_to :clan
	belongs_to :user
	belongs_to :clan_rank
  after_create :remove_invites
	after_create :set_default_rank
  # def create
  #   @clan_invite = ClanInvite.find(params[:clan_invite_id])
  #   if @clan_invite.approved?
  #     @clan_member = ClanMember.create(user: @clan_invite.user, clan: @clan_invite.clan)
  #   end
  #   redirect_to @clan_invite.clan
  # end
  # def clan_rank
  #   begin
  #     ClanRank.find(clan_rank_id)
  #   rescue
  #     clan.default_rank
  #   end
  # end

  def can_perform? action
    # Host can always perform any action, duh
    return true if clan.host == user
    clan_rank.permissions?(action.to_sym)
  end

  private
    def remove_invites
      ClanInvite.where(user_id: self.user_id, clan_id: self.clan_id).each do |invite|
        invite.destroy
        invite.clan.host.mailbox.notifications if Rails.env != "test"
      end
      if Rails.env != "test"
        subj = "#{user.username} has joined clan #{clan.name}"
        body = "#{user.username} has joined clan #{clan.name}"
        clan.host.notify(subj, body, self)
      end
    end

		def set_default_rank
			update_attributes(clan_rank_id: clan.clan_ranks.where(is_default: true).first.id)
		end

end
