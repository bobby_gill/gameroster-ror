class ClanApplication < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :clan
  belongs_to :user
  has_many :answers
  has_one :reviewer,  class_name: "User", primary_key: :reviewer_id, foreign_key: :id

  validates :clan_id, presence: true
  validates :user_id, presence: true

  accepts_nested_attributes_for :answers, :allow_destroy => true
  validates_associated :answers


  # Interesting reading
  # http://www.rubydoc.info/github/ging/mailboxer/Mailboxer/Notification
  def send_message
    subj = "'#{clan.name.upcase}' clan Received Application from '#{user.username}'"
    body = "'#{clan.name.upcase}' clan Received Application from '#{user.username}'"
    clan.host.notify(subj, body, self)
    clan.clan_members.preload(:user, :clan_rank).where('user_id != ?', clan.host_id).each do |member|
      if member.clan_rank.review_applications?
        member.user.notify(subj, body, self)
      end
    end
  end

  def pending?
    status.nil?
  end

  def accepted?
    status == true
  end

  def rejected?
    status == false
  end

  def view_status
    pending? ? 'Pending' : accepted? ? 'Accepted' : rejected? ? 'Rejected' : 'Unknow'
  end

end
