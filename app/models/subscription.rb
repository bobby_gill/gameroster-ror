class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscription_plan

  after_update :update_subscription_expiration!

  enum state: [ :pending, :active, :canceled, :suspended ]
  enum platform: [ :web, :ios, :android ]

  attr_accessor :payer_id

  delegate :name, to: :subscription_plan

  #
  # sync ends_on with users trial_expiration for active
  #
  def update_subscription_expiration!
    if active? and web? and user and ( !user.trial_expiration or ends_on > user.trial_expiration )
      user.update( trial_expiration: ends_on )
    end
  end
end
