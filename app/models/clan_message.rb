class ClanMessage < ActiveRecord::Base
    belongs_to :clan
    belongs_to :user
    
    default_scope {order(created_at: :asc)}
    validates_presence_of :message
    
    
end
