class SubscriptionPlan < ActiveRecord::Base
  enum period: [ :monthly, :yearly ]

  validates :name,        presence: true
  validates :price,       presence: true
  validates :period,      presence: true
  validates :recurring,   presence: true
end
