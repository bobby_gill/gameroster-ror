class Game < ActiveRecord::Base
	has_many :game_game_system_joins
	has_many :game_systems, through: :game_game_system_joins
	has_and_belongs_to_many :clans

	mount_uploader :game_cover, GameCoverUploader
	mount_uploader :game_jumbo, GenericUploader
	mount_uploader :game_jumbo_mobile, GenericUploader
	mount_uploader :game_logo, GenericUploader

	validates :title, presence: true
	# validates :game_cover, presence: true
	# validates :game_jumbo, presence: true
	# validates :game_logo, presence: true

	def game_title
		title
	end

end
