class Invite < ActiveRecord::Base
  belongs_to :contract
  belongs_to :roster, class_name: "Roster", foreign_key: :contract_id
  belongs_to :user

  acts_as_list scope: :contract

  validates :user, uniqueness: { scope: :contract, message: "already invited" }

  enum status: [:pending, :confirmed, :declined, :waitlisted, :no_show]

  # not waitlisted
  scope :interested, -> () { where status: [ 0, 1, 3 ] } # :pending, :confirmed, :waitlisted
  scope :rateable, -> () { where status: [ 1, 4 ] } # :confirmed, :no_show
  after_save :schedule_upcoming_notification

  def claimable?
    # can this invite be confirmed!
    ( pending? or declined? )  and !roster.full?
  end

  def declinable?
    # can this invite be declined!
    !declined?
  end

  def waitlistable?
    roster.full? and !confirmed? and !waitlisted?
  end

  def declined!
    move_to_bottom
    super
    subj = "#{user.username} has declined their invitation"
    body = "%s. [roster id=\"%d\" invited_user_id=\"%d\"]" % [ subj, roster.id, user.id ]
    roster.send_message body, subj
  end

  def waitlisted!
    move_to_bottom
    super
    subj = "#{user.username} has been placed on the waitlist"
    body = "%s. [roster id=\"%d\" invited_user_id=\"%d\"]" % [ subj, roster.id, user.id ]
    roster.send_message body, subj
  end

  def confirmed!
    super
    # send out messages
    subj = "#{user.username} has confirmed their invite"
    body = "%s. [roster id=\"%d\" invited_user_id=\"%d\"]" % [ subj, roster.id, user.id ]
    roster.send_message body, subj
  end

  def schedule_upcoming_notification
    roster.schedule_upcoming_notification
  end

end
