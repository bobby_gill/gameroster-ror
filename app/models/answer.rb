class Answer < ActiveRecord::Base
  belongs_to :clan
  belongs_to :user
  belongs_to :question,  -> { with_deleted }
  belongs_to :clan_application

  validates :clan_id, presence: true
  validates :user_id, presence: true
  validates :question_id, presence: true
  validates :answer, presence: true
end
