Shortcode.setup do |config|
  config.self_closing_tags = [:contract, :roster, :game_notice]
  config.helpers = [MessagesHelper, Rails.application.routes.url_helpers, ProfileHelper]
  Shortcode.register_presenter(ContractsPresenter, RostersPresenter, GameNoticePresenter)
end
