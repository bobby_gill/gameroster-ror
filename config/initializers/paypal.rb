PayPal::SDK.load("config/paypal.yml", Rails.env)
PayPal::SDK.logger = Rails.logger

#
#  leverage paypal.yml from adaptive payments to config recurring 
#
PayPal::Recurring.configure do | config |
  config.sandbox   = !PayPal::SDK::Core::Config.config.mode.eql?('live')
  config.username  = PayPal::SDK::Core::Config.config.username
  config.password  = PayPal::SDK::Core::Config.config.password
  config.signature = PayPal::SDK::Core::Config.config.signature
end
