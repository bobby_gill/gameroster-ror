# use s3 if defined
if ENV.has_key?('AWS_BUCKET_NAME')
  CarrierWave.configure do |config|
    config.storage    = :aws
    config.aws_bucket = ENV.fetch('AWS_BUCKET_NAME')
    # config.aws_acl    = :public_read
    # config.asset_host = 'http://example.com'
    # config.aws_authenticated_url_expiration = 60 * 60 * 24 * 365
     
    config.aws_credentials = {
      access_key_id:     ENV.fetch('AWS_ACCESS_KEY_ID'),
      secret_access_key: ENV.fetch('AWS_SECRET_ACCESS_KEY')
    }

  end
else
  CarrierWave.configure do |config|
    config.storage :file
  end
end
 

