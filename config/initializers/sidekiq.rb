# if REDIS_URL is not defined, it will default to localhost 
Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_URL'] }
end
