if ENV["PARSE_ENABLED"].present? and ENV["PARSE_ENABLED"].eql?('true')
  Parse.init :application_id => ENV["parse_application_id"], :api_key => ENV["parse_api_key"],:quiet => false
end
