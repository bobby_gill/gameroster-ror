set :stage, :staging

def current_git_branch
  branch = `git symbolic-ref HEAD 2> /dev/null`.strip.gsub(/^refs\/heads\//, '')
end

set :branch, ask('branch name: ', current_git_branch)

set :rails_env, "production"
server 'ec2-52-44-122-230.compute-1.amazonaws.com', user: 'gamemercs', roles: %w{web app db}, primary: true

set :unicorn_workers, 2
set :rvm_type, :user
set :rvm_ruby_version, '2.2.1@game-mercenaries'

# current production credentials in code, need to override 
set :linked_files, fetch(:linked_files, []).push('config/environments/production.rb', 'config/paypal.yml')
