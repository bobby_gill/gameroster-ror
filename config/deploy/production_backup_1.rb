# Unicorn/Nginx
# =============
# ignore this if you do not need SSL
# set :nginx_use_ssl, true
# set :nginx_ssl_cert_local_path, '/path/to/ssl_cert.crt'
# set :nginx_ssl_cert_key_local_path, '/path/to/ssl_cert.key'
set :unicorn_workers, 4
set :rails_env, "production"

# RVM 
# ===
set :rvm_type, :user
set :rvm_ruby_version, '2.2.1@game-mercenaries'

# Extended Server Syntax
# ======================
#server 'game-mercenaries-web-1', roles: %w{web app}

role :web, ["game-mercenaries-web-3"]
role :app, ["game-mercenaries-web-3"]

