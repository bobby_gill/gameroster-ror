set :stage, :production
set :branch, 'staging'

set :rails_env, "production"
server '54.146.28.179', user: 'gamemercs', roles: %w{web app db}, primary: true
set :assets_roles, [:web, :app]

set :unicorn_workers, 2
set :rvm_type, :user
set :rvm_ruby_version, '2.2.1@game-mercenaries'

# current production credentials in code, need to override 
# set :linked_files, fetch(:linked_files, []).push('config/environments/production.rb', 'config/paypal.yml')