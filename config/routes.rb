require 'sidekiq/web'
Rails.application.routes.draw do

	scope module: 'clans' do
	  resources :clans do
	    resources :clan_messages, shallow: true
			resources :clan_members, shallow: true
			resources :clan_messages, shallow: true
			resources :clan_ranks, shallow: true
			resources :clan_events, only: [:index]
			resources :clan_links, only: [:index]
			resources :clan_contacts, only: [:create]
			resources :clan_applications, only: [:index, :destroy, :create, :show] do
				patch 'accept' => 'clan_applications#accept', as: :accept, on: :member
			end
			resources :clan_donations, only: [:create] do
				post 'ipn_notification' => 'clan_donations#ipn_notification', on: :member
				get  'thanks' => 'clan_donations#thanks', on: :member
			end
			patch 'change_owner' => 'clans#change_owner', on: :member
			patch 'unblock_member/:clan_member_id' => 'clans#unblock_member', on: :member
			patch 'clan_members/:clan_member_id' => 'clans#clan_members_update', on: :member
			delete 'remove_member/:clan_member_id' => 'clans#remove_member', on: :member
	  end
	  resources :clan_invites
	  post 'clan_invites/:clan_invite_id/accept' => 'clan_invites#accept', as: 'accept_clan_invite'
	end

  get 'clan_notices/:clan_id/new' => 'clans/clan_notices#new', as: 'new_clan_notice'
  get 'clan_notices/:id' => 'clans/clan_notices#show', as: 'clan_notice'
  post 'clan_notices/:clan_id/new' => 'clans/clan_notices#create'
  delete 'clan_notices/:id' => 'clans/clan_notices#destroy', as: 'delete_clan_notice'

  get 'system_notices/new' => 'system_notices#new', as: 'new_system_notice'
  get 'system_notices/:id' => 'system_notices#show', as: 'system_notice'
  get 'system_notices' => 'system_notices#index', as: 'system_notices'
  post 'system_notices/new' => 'system_notices#create'
  delete 'system_notices/:id' => 'system_notices#destroy', as: 'delete_system_notice'

	resources :announcements, only: :index
	post 'announcements/read' => 'announcements#read'

	devise_for :admins, controllers: { sessions: 'admin/sessions' }
	devise_for :users,  controllers: { sessions: 'user/sessions', registrations: 'user/registrations', confirmations: 'user/confirmations' }

	authenticate :admin do
		mount Sidekiq::Web => '/sidekiq'
  end

	authenticated :admin do
		root to: 'admin/dashboard#index', as: :authenticated_admin
		namespace :admin do
			resources :games
			resources :game_systems
			resources :charities
			resources :cancellations
		  resources :users
			resources :rosters
			resources :reports
			resources :events, only: :index
			get 'cancellations/(:rosters)' => 'cancellations#index'
			patch 'cancellations/:id/uncancel' => 'cancellations#uncancel'
			post 'users/:id/become/(:prev_action)' => 'users#become', as: 'user_become'
			get 'users/:id/confirm' => 'users#confirm', as: 'user_confirm'
			get 'users/:id/account'  => 'users#account', as: 'user_account'
		end
	end

	authenticated :user do
		root to: 'dashboard#index', as: :authenticated_user
	end

	unauthenticated do
		devise_scope :user do
			root to: 'pages#home', as: :unauthenticated_user
			get 'tosmodal' => 'user/registrations#tosmodal', as: 'tosmodal'
		end
	end

	#the following resources are actually all the same model - the user.
	#it just made breaking up the CRUD for the user easier because we're using
	#inherited resources.
	resource :user, path: 'profile', controller: 'profile/profile', as: 'profile'
	namespace :profile do
		resource :user, path: 'country', controller: 'country', as: 'country', only: [ :edit, :update ]
		resource :user, path: 'account', controller: 'account', as: 'account'
		resource :user, path: 'games', controller: 'games_and_systems', as: 'games_and_systems'
		resources :blocks
		resources :favorites
                resource :notifications, only: [:edit, :update]
		get 'avatar' => 'avatar#index'
		get 'subscription' => 'subscription#index'
                post 'add-game-modal' => 'games_and_systems#add_game', as: 'add_game'
                get 'check-user-system' => 'games_and_systems#check_game', as: 'check_game'
	end

	#the following both deal with the contracts controller - my-contracts is
	#obviously just scoped to the current user.
	get 'contracts/close_pay_pal' => 'contracts/contracts#close_pay_pal'

	resources :contracts, controller: 'contracts/contracts', path: 'looking-for-players'

	resources :contracts, controller: 'players/players', path: 'players', as: 'players'

	resources :contracts, controller: 'contracts/contracts', only: [] do
		#we're going to break the REST model as paypal doesn't allow sending
		#post/patch method via the return url.  We'll have to confirm the payment
		#from paypal via IPN anyways, but the following purchase method will
		#just set the status of contract to pending payment
		get 'purchase' => 'contracts/contracts#purchase'

		#IPN url
		post 'ipn_notification' => 'contracts/contracts#ipn_notification'

		post  'payment_request' => 'contracts/contracts#payment_request', as: 'payment_request'

		post 'claim' => 'contracts/contracts#claim'

	end

	namespace :subscriptions do
		get 'abort'
		get 'cancel'
		get 'ipn_notification'
	end

	resources :subscription_plans, only: [] do
    	resources :subscriptions, shallow: true
  	end

	get 'multiple_times' => 'contracts/posted_contracts#multiple_times_new', as: 'multiple_times_new'
	post 'multiple_times' => 'contracts/posted_contracts#multiple_times_create', as: 'multiple_times_create'

	resources :contracts, path: 'my-posted-events', controller: 'contracts/posted_contracts', as: 'posted_contracts' do
		get 'cancel' => 'contracts/posted_contracts#cancel', as: 'cancel'
		patch 'rate' => 'contracts/posted_contracts#rate', as: 'rate'
		patch 'claim' => 'contracts/posted_contracts#claim', as: 'claim'
		patch 'deny' => 'contracts/posted_contracts#deny', as: 'deny'
	end

	resources :contracts, path: 'my-claimed-events', controller: 'contracts/claimed_contracts', as: 'claimed_contracts' do
		get 'cancel' => 'contracts/claimed_contracts#cancel', as: 'cancel'
		patch 'rate' => 'contracts/claimed_contracts#rate', as: 'rate'
	end

	resources :rosters do
		get 'cancel', as: 'cancel', on: :member
		get 'add_invitees' => 'rosters#add_invitees', on: :member
    	resources :roster_messages, shallow: true
    	resources :invites, only: [ :create ]
	end

	resources :roster_invites, only: [:create]

	resources :invites, only: [:destroy] do
		patch 'claim', on: :member
		patch 'decline', on: :member
		patch 'waitlist', on: :member
		patch 'no_show', on: :member
		patch 'reconfirm', on: :member
		put  :update_position, on: :member
	end

	resources :events, path: 'my-events', only: :index
	resource :events do
		get 'view_all' => 'events#view_all'
	end
	resources :events, only: :show

	get 'add_invitees' => 'rosters#add_invitees', as: 'add_invitees'
	post 'add_invitees_create' => 'invites#add_invitees_create', as: 'add_invitees_create'

	get 'add_game_modal' => 'dashboard#add_game_modal'

	#the following both deal with the bounties controller - my-bounties is
	#obviously just scoped to the current user.
	get 'bounties/close_pay_pal' => 'bounties/bounties#close_pay_pal'
	get 'bounties/feed' => 'bounties/bounties#feed'

	resources :bounties, controller: 'bounties/bounties', path: 'public-games'

	resources :bounties, controller: 'bounties/bounties', only: []  do
		#we're going to break the REST model as paypal doesn't allow sending
		#post/patch method via the return url.  We'll have to confirm the payment
		#from paypal via IPN anyways, but the following purchase method will
		#just set the status of bounty to pending payment
		get 'purchase' => 'bounties/bounties#purchase'

		#IPN url
		post 'ipn_notification' => 'bounties/bounties#ipn_notification'

		post  'payment_request' => 'bounties/bounties#payment_request', as: 'payment_request'

		post 'claim' => 'bounties/bounties#claim'
	end


	resources :bounties, path: 'my-posted-bounties', controller: 'bounties/posted_bounties', as: 'posted_bounties' do
		get 'cancel' => 'bounties/posted_bounties#cancel', as: 'cancel'
		patch 'rate' => 'bounties/posted_bounties#rate', as: 'rate'
		patch 'claim' => 'bounties/posted_bounties#claim', as: 'claim'
		patch 'deny' => 'bounties/posted_bounties#deny', as: 'deny'
	end
	resources :bounties, path: 'my-claimed-bounties', controller: 'bounties/claimed_bounties', as: 'claimed_bounties' do
		get 'cancel' => 'bounties/claimed_bounties#cancel', as: 'cancel'
		patch 'rate' => 'bounties/claimed_bounties#rate', as: 'rate'
	end

	resources :profiles do
		get 'blocks-and-feedback' => 'profiles#blocks_and_feedback'
		get 'favorites' => 'profiles#favorites'
		get 'feedback' => 'profiles#feedback'
		get 'contracts' => 'profiles#contracts'
    resource :donation, only: [:new, :create, :index ]
    resources :ratings, shallow: true, except: [ :destroy ]
	end

	resources :donations, only: []  do
		post 'ipn_notification' => 'donations#ipn_notification', on: :member
		get  'thanks' => 'donations#thanks', on: :member
	end

	resources :charities do
		get 'charities' => 'charities#index'
	end

	get 'messages/trash' => 'messages#trash', as: "trash_messages"
	resources :messages do

	end
	get 'notifications' => 'messages#notifications'

	get 'invite_user' => 'rosters#invite_user'

	get 'dashboard' => 'dashboard#index', as: 'dashboard'

	get 'games' => 'games#index', as: 'games'
	get 'about' => 'pages#about', as: 'about'
	get 'leaderboard' => 'pages#leaderboard', as: 'leaderboard'
	get 'tos' => 'pages#tos', as: 'tos'
	get 'privacy' => 'pages#privacy', as: 'privacy'
	get 'thankyou' => 'pages#thankyou', as: 'thankyou'
	get 'faq' => 'pages#faq', as: 'faq'
	get 'how_to' => 'pages#how_to', as: 'how_to'
	get 'policies' => 'pages#policies', as: 'policies'
	get 'PUR' => 'pages#gamestop', as: 'gamestop'
	get 'google0dad1a6d425ae80a.html' => 'pages#google_verification'
	post 'default-ipn-notification' => 'pages#default_ipn_notification'
	get 'destiny-lfg' => 'pages#destiny_landing'
	get 'updates' => 'pages#updates', as: 'updates'

	#this will be moved into events resource flow but I don't know how
	get 'share' => 'pages#share', as: 'share'

	# resources :contact_requests, :only => [:index, :create, :new],
	get 'contact' => 'contact_requests#new', as: 'contact_requests'
	post 'contact' => 'contact_requests#create'

	get 'password' => 'site_password#index', as: 'site_password'
	post 'password' => 'site_password#create'
	get 'welcome' => 'site_password#welcome', as: 'site_password_welcome'
	get 'search' => 'mercenaries#index', as: 'mercenaries'

	%w( 404 422 500 503 ).each do |code|
		get code, :to => "errors#show", :code => code
	end

	# You can have the root of your site routed with "root"
	root 'home#index'

	#--------------------------------------
	# API
	#--------------------------------------
	namespace :api, defaults: { format: :json } do
		namespace :v1 do
			devise_scope :user do
				match '/user/sessions' => 'user/sessions#create', :via => :post
				match '/user/sessions' => 'user/sessions#destroy', :via => :delete
			end
			get 'forwarder' => 'forwarder#forward'

			resources :rosters do
	    	resources :roster_messages, shallow: true, only: [:index, :create]
				resources :invites, only: [ :create ]
			end
			resources :bounties, controller: 'bounties/bounties', path: 'public-games'
			resources :contracts, controller: 'contracts/contracts', only: [:index, :show] do
				post 'claim' => 'contracts/contracts#claim'
			end
			resources :events, path: 'my-events', only: :index
			resources :contracts, path: 'my-posted-events', controller: 'contracts/posted_contracts', as: 'posted_contracts' do
				patch 'claim' => 'contracts/posted_contracts#claim', as: 'claim'
			end
			resources :contracts, path: 'my-claimed-events', controller: 'contracts/claimed_contracts', as: 'claimed_contracts', only: :destroy

			resources :messages
			resources :profiles, only: :show
			namespace :profile do
				resources :blocks
				resources :favorites
		    resource :notifications, only: [:update]
			end
			get 'search' => 'mercenaries#index', as: 'mercenaries'
			resources :devices
			resources :reports, only: [:create]
			resources :game_game_systems, only: [:index]
			resources :subscriptions, only: [:create]
			resources :subscription_plans, only: [:index]
			resources :invites, only: [:index, :destroy] do
				patch 'claim', on: :member
				patch 'decline', on: :member
			end
		end
	end
end
