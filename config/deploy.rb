# config valid only for current version of Capistrano
lock '3.7.2'

set :application, 'game-mercenaries'
set :repo_url, 'git@github.com:bacancy/game_roster.git'

set :deploy_to, '/home/gamemercs/rails_apps/game-mercenaries'
set :pty, true
set :use_sudo, true

set :scm, :git
set :format, :pretty
set :log_level, :debug

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/application.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :ssh_options, { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }

set :bundle_binstubs, nil

set :keep_releases, 5

#after "deploy:updated", "newrelic:notice_deployment"


task :restart_sidekiq do
  on roles(:app) do
    execute "/sbin/service sidekiq_game-mercenaries_production stop"
    execute "/sbin/service sidekiq_game-mercenaries_production start"
  end
end
after "deploy:published", "restart_sidekiq"

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end