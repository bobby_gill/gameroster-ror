source 'https://rubygems.org'
ruby '2.2.1'

# Standard Rails gems
gem 'rails', '4.2.1'
gem 'uglifier'
gem 'jquery-rails'
gem 'jbuilder'
gem 'bcrypt'


# Kaminari: https://github.com/amatsuda/kaminari
gem 'kaminari'

gem 'pg'
gem 'inherited_resources'
gem 'multi_logger'

gem 'bitmask_attributes'
gem 'squeel'


group :development, :test do
  gem 'quiet_assets'
  gem 'byebug'
  gem 'web-console'
	gem 'jazz_fingers'
  # Spring: https://github.com/rails/spring
  gem 'spring'
  gem 'minitest-rails'
  gem 'guard'
  gem 'guard-minitest'
  gem 'awesome_print'
  #
end

# Figaro: https://github.com/laserlemon/figaro
gem 'figaro'

# Devise: https://github.com/plataformatec/devise
gem 'devise'
gem 'simple_token_authentication', '~> 1.0'


gem 'therubyracer'
gem 'coffee-rails'
gem 'bootstrap-sass'
gem 'sass-rails'
gem 'bootstrap_form'
gem "compass-rails", github: "Compass/compass-rails", branch: "master"

# Split large application style sheet into multiple files so IE9 doesn't hit max selector limit
gem 'css_splitter'

# testing
group :test do
  gem 'simplecov', :require => false
  gem 'simplecov-rcov', :require => false
  gem 'ci_reporter_minitest', '~> 1.0.0'
end

# development only
group :development do
  # check for rails vulnerabilities
  gem 'brakeman', :require => false

  group :darwin do
    #  Growl notification
    gem 'growl'
  end

  # generate fake data that looks kind real
  gem 'faker'
end

#modified version of wice to use our nice jquery-datetimepicker
gem "wice_grid", github: 'awanosik/wice_grid', ref: '1af9f4f333ed320fba4c9bcb0fe7d985b5a47537'

#file uploads
gem 'carrierwave'
# enable cloud file storage s3, google, etc
gem 'carrierwave-aws'
# support for ajax multipart
gem 'remotipart'

#gimme imagemagick
gem 'rmagick'

#Nice Time Picker https://github.com/Envek/jquery-datetimepicker-rails
gem 'jquery-datetimepicker-rails'

# for having environment config options
gem 'rails_config'

# we store UTC, let the browser set the timezone
gem 'browser-timezone-rails'

# be able to parse MM/DD/YEAR instead of default DD/MM/YEAR
gem 'american_date'

#https://github.com/tsechingho/chosen-rails
gem 'chosen-rails'

#https://github.com/bokmann/fullcalendar-rails
gem 'fullcalendar-rails'
gem 'momentjs-rails'

#https://github.com/paypal/adaptivepayments-sdk-ruby
gem 'paypal-sdk-adaptivepayments'
gem 'paypal-recurring'

#https://github.com/chepri/mailboxer  - Chepri fork, allows for
#conversationables, see: http://stackoverflow.com/questions/22914420/adding-belongs-to-relationship-to-ruby-gem-mailboxer
gem 'mailboxer', github: 'chepri/mailboxer', ref: 'ec300d876753ae370018aa907949f968b1925136'

#https://github.com/balexand/email_validator
gem 'email_validator'

#https://github.com/codegram/date_validator
gem 'date_validator'

# https://github.com/kernow/shortcode
gem 'shortcode'

gem 'ice_cube'

# ISO 3166 countries and state/subdivisions
gem 'countries'

#chat
gem 'faye'
gem 'thin', require: false
gem 'render_sync'

# Unicorn: http://unicorn.bogomips.org
group :production do
  gem 'unicorn'
  gem 'rails_12factor'
  gem 'newrelic_rpm'
end

#soft deletion
gem "paranoia", "~> 2.2"

# sorting and ordering associations
gem 'acts_as_list'

# API model serialization
gem 'active_model_serializers'

# Parse REST API clients https://github.com/adelevie/parse-ruby-client
gem "parse-ruby-client"

# used by the notification worker
gem 'sidekiq'
gem 'sinatra', :require => nil

# Use Capistrano for deployment
gem 'capistrano-rvm', require: false
group :development do
  # Competely breaks json response for inviting users
  #gem 'meta_request'
	gem 'capistrano' , require: false
	gem 'capistrano-rails',   '~> 1.1', require: false
	gem 'capistrano-bundler', '~> 1.1', require: false
	gem 'capistrano-unicorn-nginx' , github: 'chepri/capistrano-unicorn-nginx', ref: 'cb626b59504174db4b78dc6284df98c9634ae93f'
	gem 'capistrano-sidekiq'

  # log api calls
  gem 'httplog'
end

#itunes receipt validation
gem 'itunes-receipt'
gem 'kappa'
gem 'browser'
