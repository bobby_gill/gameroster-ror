module Notifications
	class ContractNotification < Notifications::BaseNotification
		# Sends a notification about a user claiming
		def self.claimed(contract)
      user = contract.buyer
      contract_owner = contract.owner
      device_tokens = []

      if user.notif_push && user.notif_system
        tokens = contract_owner.devices.where :device_type => 'ios'
        if tokens.count > 0
          device_tokens += tokens.pluck :device_token
        end

  			action = 'claimed your contract'
  			start_date_time = contract.start_date_time.in_time_zone user.timezone

        push_text = "%s has %s | %s (%s) | %s at %s" % [ user.username, action, contract.selected_game_game_system_join.game.title, contract.selected_game_game_system_join.game_system.abbreviation, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

        push_notification = {
  				:method => :contract_claimed,
  				:sound => 'default',
  				:alert => push_text,
  				:recipients => device_tokens
  			}
  			ContractNotification.new(contract, {}, {}, push_notification).deliver
      end
		end

		def self.upcoming(contract)
			ggsj = contract.roster? ? contract.game_game_system_joins.first : contract.selected_game_game_system_join
			# send notifications in their timezones
			all_users = if contract.contract? || contract.bounty?
				[contract.buyer, contract.seller]
			else
				r = Roster.find contract.id
				r.confirmed_users + [r.buyer]
			end
			all_users.each do | u |
				if u.notif_reminder
          if u.notif_email
            ApplicationMailer::send_reminder_email( u, ggsj, contract ).deliver_later
          end

          if u.notif_push
  		      tokens = u.devices.where :device_type => 'ios'
  		      if tokens.count > 0
  		        device_tokens = tokens.pluck :device_token
  						start_date_time = contract.start_date_time.in_time_zone u.timezone
  			      push_text = "You have an upcoming event | %s (%s) | %s at %s" % [ ggsj.game.title, ggsj.game_system.abbreviation, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

  			      push_notification = {
  							:method => :contract_claimed,
  							:sound => 'default',
  							:alert => push_text,
  							:recipients => device_tokens
  						}
  						ContractNotification.new(contract, {}, {}, push_notification).deliver
            end
		      end
				end
			end
		end
  end
end
