module Notifications
	class MessageNotification < Notifications::BaseNotification
		def self.conversation_reply(message)
			# sms = {
			# 	:method => :new_alert,
			# 	:body  => "New #{alert.alert_category.name} Alert. View at #{Rails.application.routes.url_helpers.url_for(:controller => 'organization/alerts', :action => 'show', :id => alert.id)}",
			# 	:recipients => User.where(sms_alert_created: true).where(:organization => listeners)
			# }
			conversation = message.conversation
      reply_sender = message.sender.username

      receipts = conversation.messages.last.receipts
      device_tokens = []
      receipts.each do |r|
        receiver = r.receiver
        if receiver.username != reply_sender
          if receiver.notif_push && receiver.notif_system
            tokens = receiver.devices.where :device_type => 'ios'
            if tokens.count > 0
              device_tokens += tokens.pluck :device_token
            end
          end
        end
      end

      push_text = message.body.gsub(/\[[^\]]*\]/, '')
      push_text = push_text[0..39] + '...' if push_text.length > 40

      push_notification = {
				:method => :conversation_reply,
				:sound => 'default',
				:alert => "Reply from #{reply_sender}: #{push_text}",
				:recipients => device_tokens
			}
			MessageNotification.new(message, {}, {}, push_notification).deliver
		end

    def self.new_conversation(message)
      # sms = {
      # 	:method => :new_alert,
      # 	:body  => "New #{alert.alert_category.name} Alert. View at #{Rails.application.routes.url_helpers.url_for(:controller => 'organization/alerts', :action => 'show', :id => alert.id)}",
      # 	:recipients => User.where(sms_alert_created: true).where(:organization => listeners)
      # }
      conversation = message.conversation
      message_sender = message.sender.username

      receipts = conversation.messages.last.receipts
      device_tokens = []
      receipts.each do |r|
        receiver = r.receiver
        if receiver.username != message_sender
          if receiver.notif_push && receiver.notif_system
            tokens = receiver.devices.where :device_type => 'ios'
            # TODO: check notification settings to make sure they want push
            if tokens.count > 0
              device_tokens += tokens.pluck :device_token
            end
          end
        end
      end

      push_text = message.body.gsub(/\[[^\]]*\]/, '')
      push_text = push_text[0..39] + '...' if push_text.length > 40

      push_notification = {
        :method => :conversation_reply,
        :sound => 'default',
        :alert => "New conversation started by #{message_sender}: #{push_text}",
        :recipients => device_tokens
      }
      MessageNotification.new(message, {}, {}, push_notification).deliver
    end

  end
end
