module Notifications
	class EventInviteNotification < Notifications::BaseNotification
		def self.invited(invite)
      event = invite.roster
      user = invite.user
      event_owner = event.owner.username

      if user.username != event_owner
  			if user.notif_push && user.notif_system
          tokens = user.devices.where :device_type => 'ios'
          if tokens.count > 0
            device_tokens = tokens.pluck :device_token
    				start_date_time = event.start_date_time.in_time_zone user.timezone
            push_text = "Invite from %s | %s (%s) | %s at %s" % [ event.owner.username, event.contract_game_game_system_joins.first.game_game_system_join.game.title, event.contract_game_game_system_joins.first.game_game_system_join.game_system.abbreviation, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

            push_notification = {
                :method => :invited,
                :sound => 'default',
                :alert => push_text,
                :recipients => device_tokens
            }
            EventInviteNotification.new(event, {}, {}, push_notification).deliver
          end
  			end
      end
		end

		# Sends a notification about a user joining the event, or joining the waitlist
		def self.joined(invite)
			event = invite.roster
      user = invite.user
      event_owner = event.owner
      device_tokens = []

			if event_owner.notif_push && user.notif_system
	      tokens = event_owner.devices.where :device_type => 'ios'
	      if tokens.count > 0
	        device_tokens += tokens.pluck :device_token
	      end

				action = invite.confirmed? ? 'claimed a spot' : 'joined the waitlist'

				start_date_time = event.start_date_time.in_time_zone event_owner.timezone
	      push_text = "%s has %s | %s (%s) | %s at %s" % [ user.username, action, event.contract_game_game_system_joins.first.game_game_system_join.game.title, event.contract_game_game_system_joins.first.game_game_system_join.game_system.abbreviation, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

	      push_notification = {
					:method => :invited,
					:sound => 'default',
					:alert => push_text,
					:recipients => device_tokens
				}
				EventInviteNotification.new(event, {}, {}, push_notification).deliver
			end
		end

		def self.time_changed(invite)
			event = invite.roster
      user = invite.user
      event_owner = event.owner.username
      device_tokens = []

      if user.username != event_owner
        if user.notif_push
          tokens = user.devices.where :device_type => 'ios'
          if tokens.count > 0
            device_tokens += tokens.pluck :device_token
          end

          start_date_time = event.start_date_time.in_time_zone user.timezone
          push_text = "Time has been updated for %s's Roster Event. You will need to re-claim your spot on the roster | %s at %s" % [ event.owner.username, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

          push_notification = {
            :method => :invited,
            :sound => 'default',
            :alert => push_text,
            :recipients => device_tokens
          }
          EventInviteNotification.new(event, {}, {}, push_notification).deliver
        end
      end
		end

		def self.moved_from_waitlist(invite)
			event = invite.roster
			user = invite.user
			event_owner = event.owner.username
			device_tokens = []

			if user.username != event_owner
        if user.notif_push
  				tokens = user.devices.where :device_type => 'ios'
  				if tokens.count > 0
  					device_tokens += tokens.pluck :device_token
  				end

          start_date_time = event.start_date_time.in_time_zone user.timezone
          push_text = "You have been confirmed from a waitlist | %s (%s) | %s at %s" % [ event.owner.username, event.contract_game_game_system_joins.first.game_game_system_join.game.title, event.contract_game_game_system_joins.first.game_game_system_join.game_system.abbreviation, (start_date_time.strftime "%m/%d"), (start_date_time.strftime "%l:%M%P") ]

          push_notification = {
            :method => :invited,
            :sound => 'default',
            :alert => push_text,
            :recipients => device_tokens
          }
          EventInviteNotification.new(event, {}, {}, push_notification).deliver
        end
			end
		end
  end
end
