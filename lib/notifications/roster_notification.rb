module Notifications
	class RosterNotification < Notifications::BaseNotification
		def self.send_notification(roster)
      roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.each do | u |
        if u.notif_email
          ApplicationMailer::send_public_game_announcement( u, roster ).deliver_later
        end

        if u.notif_push
          device_tokens = []
          tokens = u.devices.where :device_type => 'ios'
          if tokens.count > 0
            device_tokens += tokens.pluck :device_token
          end

          Time.use_zone( u.timezone) do
            push_text = "New Public Game. Starting at %s" % [ roster.start_date_time.in_time_zone.strftime("%m/%d %l:%M%p %Z") ]

            push_notification = {
            :method => :invited,
            :sound => 'default',
            :alert => push_text,
            :recipients => device_tokens
          }
          RosterNotification.new(roster, {}, {}, push_notification).deliver
          end
        end
      end
		end
  end
end
