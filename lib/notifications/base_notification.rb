module Notifications
	class BaseNotification
		def initialize resource, email = {}, sms = {}, push_notification = {}
			@resource = resource
			@email = email
			@sms = sms
			@push_notification = push_notification
		end

		def deliver
			deliver_email if ENV["MAIL_ENABLED"] == "true"
			deliver_sms if ENV["TWILIO_ENABLED"] == "true"
			deliver_push_notification if ENV["PARSE_ENABLED"].present? and ENV["PARSE_ENABLED"].eql?('true')
		end

		private

		def filter_sms_recipients(unfiltered_users)
			users = []
			to_skip_area_codes = [
				'800', '888', '877', '866', '855', '844', '833', '822', '880', '881', '882',
				'883', '884', '885', '886', '887', '889'
			]
			unfiltered_users.where.not(:phone_number => '').each do |u|
				# we don't want to send to people with 1-800 type numbers
				clean = u.phone_number.gsub(/[^0-9]/, '')
				clean = clean[1..-1] if clean[0] == '1'
				# we're assuming US numbers for now
				users.push u if clean.length == 10 && to_skip_area_codes.find_index(clean[0..2]) == nil
			end
			users
		end

		def deliver_email
			return if !@email[:recipients].present?

			@email[:recipients].each do |recipient|
				ApplicationMailer.send(@email[:method], @resource, recipient.email).deliver_now
			end
		end

		def deliver_sms
			return if !@sms[:recipients].present?

			client = Twilio::REST::Client.new ENV["twilio_account_sid"], ENV["twilio_auth_token"]
			callback_url = Rails.application.routes.url_helpers.url_for(:controller => 'notifications_callback', :action => 'twilio')
			sms_recipients = filter_sms_recipients(@sms[:recipients])
			sms_recipients.each do |recipient|
				text_notification = TextNotification.new
				text_notification.phone_number = recipient.phone_number
				text_notification.message = @sms[:body]
				text_notification.user = recipient
				text_notification.alert = @resource
				# this isn't useful for now, but will be when we do actually use a worker to send them
				text_notification.status = TextNotification.statuses[:queued]

				begin
					message = client.account.messages.create(
						:body => @sms[:body],
						:to => recipient.phone_number,
						:from => ENV["twilio_phone_number"],
						:status_callback => callback_url
					)
					# callback from Twilio will update this status
					text_notification.status = TextNotification.statuses[:sending]
					text_notification.sid = message.sid
				rescue => error
					text_notification.status = TextNotification.statuses[:exception]
					recipient.disable_sms_notifications
					recipient.save
				end

				text_notification.save
			end
		end

		def deliver_push_notification
			return if !@push_notification[:recipients].present?

			query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).value_in('deviceToken', @push_notification[:recipients])
			@push_notification[:recipients] = nil
			push = Parse::Push.new(@push_notification)
			push.where = query.where
			push.save
		end
	end
end
