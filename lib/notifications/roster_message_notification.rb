module Notifications
	class RosterMessageNotification < Notifications::BaseNotification
		def self.created(message)
      sender_username = message.user.username
      event = message.roster
      event_owner = event.owner

      invites = message.roster.invites
      device_tokens = []
      invites.each do |i|
        user = i.user
        if user.username != sender_username
          if user.notif_push
            tokens = user.devices.where :device_type => 'ios'
            if tokens.count > 0
              device_tokens += tokens.pluck :device_token
            end
          end
        end
      end
			
      # event owner
			if sender_username != event_owner.username
        if event_owner.notif_push && event_owner.notif_system
  	      tokens = event_owner.devices.where :device_type => 'ios'
  	      if tokens.count > 0
  	        device_tokens += tokens.pluck :device_token
  	      end
        end
			end

      push_text = "New message in %s from %s: %s" % [ event.title, sender_username, message.message ]
      push_text = push_text[0..231] + '...' if push_text.length > 235

      push_notification = {
				:method => :conversation_reply,
				:sound => 'default',
				:alert => push_text,
				:recipients => device_tokens
			}
			MessageNotification.new(message, {}, {}, push_notification).deliver
		end
  end
end
