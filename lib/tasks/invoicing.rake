namespace :invoice do

	desc "Set Invoicing/Complete Status and send messages"
	task invoice: [:environment] do
		Bounty.where(contract_type: 'Bounty').uninvoiced.each do | b |
			if b.buyer.present? and b.seller.present?
				if b.price_in_cents == 0
					b.status = "Complete"
					#b.seller.send_message(b.buyer, "Bounty Complete [contract id=\"#{b.id}\"]", 'Bounty Complete', true, nil, Time.now, b)
				else
					b.status = "Invoiced"
					#b.seller.send_message(b.buyer, "Bounty Invoice [contract id=\"#{b.id}\"]", 'Bounty Invoice', true, nil, Time.now, b)
				end
			else
				#b.buyer.send_message(b.buyer, "Bounty Expired [contract id=\"#{b.id}\"]", 'Bounty Expired', true, nil, Time.now, b)
				b.status = 'Expired'
			end
      # at this point we do not want to be halted by any business rules that may have changed just save it no matter what
			b.save!(validate: false)
			b.buyer.update_contracts_completed! if b.buyer.present?
			b.seller.update_contracts_completed! if b.seller.present?

      # If we just invoiced a contract, lets send a donation request letter
      begin
        ApplicationMailer.send_donation_email(b).deliver_now if b.status.eql?("Invoiced")
      rescue
        Rails.logger.error "**** Problem send_donation_email for contract_id: %d" % b.id
      end
		end

		Contract.where(contract_type: 'Contract').uninvoiced.each do | c |
			if c.buyer.present? and c.seller.present?
				if c.price_in_cents == 0
					c.status = "Complete"
					#c.seller.send_message(c.buyer, "Contract Complete [contract id=\"#{c.id}\"]", 'Contract Complete', true, nil, Time.now, c)
				else
					c.status = "Invoiced"
					#c.seller.send_message(c.buyer, "Contract Invoice [contract id=\"#{c.id}\"]", 'Contract Invoice', true, nil, Time.now, c)
				end

			else
				#c.seller.send_message(c.seller, "Contract Expired [contract id=\"#{c.id}\"]", 'Contract Expired', true, nil, Time.now, c)
				c.status = 'Expired'
			end
      # at this point we do not want to be halted by any business rules that may have changed just save it no matter what
			c.save!(validate: false)
			# update number of contracts completed
			c.buyer.update_contracts_completed! if c.buyer.present?
			c.seller.update_contracts_completed! if c.seller.present?

      # If we just invoiced a contract, lets send a donation request letter
      begin
        ApplicationMailer.send_donation_email(c).deliver_now if c.status.eql?("Invoiced")
      rescue
        Rails.logger.error "**** Problem send_donation_email for contract_id: %d" % c.id
			end
	  end

    Roster.where(contract_type: 'Roster').uninvoiced.each do | roster |
      if roster.invites.confirmed.any?
        roster.complete!
      else
        roster.expired!      
      end
    end

  end

end
