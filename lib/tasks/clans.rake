# Compile some stats on the clan so that we aren't querying these on the fly
namespace :clans do



  namespace :update do 
   desc "Run all update tasks for clans"
   task :all  => :environment do
    Rake::Task["clans:calculate_activity_level"].execute
    Rake::Task["clans:calculate_average_age"].execute
    end
  end

  desc "Calculate clans activity level"
  task :calculate_activity_level  => :environment do
    Rake::Task["utils:set_logger"].execute
    Rails.logger.info "\n\n------\nGetting Clans....\n------\n"
    clans = Clan.joins(:rosters).uniq
    Rails.logger.info "\n\n------\nCalculating activity level for clans...\n------\n"
    clan_count = 0

    clans.each do |c| 
      c.activity_level = activity_level(c)
      clan_count += 1 if c.save 
      if clan_count > 0 && clan_count % 100 == 0
        Rails.logger.info "------#{clan_count} activity levels calculated------"
      end
    end
    Rails.logger.info "------Finished, #{clan_count} clan activity levels calculated ------"
  end 


desc "Calculate clans average age"
task :calculate_average_age  => :environment do
  Rake::Task["utils:set_logger"].execute
  Rails.logger.info "\n\n------\nGetting Clans....\n------\n"

  clans = Clan.joins(:users).uniq
  Rails.logger.info "\n\n------\nCalculating average age for clans...\n------\n"

  clan_count = 0
  clans.each do |c| 

    c.average_age = average_age(c)
    clan_count += 1 if c.save 
    if clan_count > 0 && clan_count % 100 == 0
      Rails.logger.info "------#{clan_count} activity ages calculated------"
    end
  end
  Rails.logger.info "------Finished, #{clan_count} clan average ages calculated ------"
end 
end







def activity_level(clan)
  relevant_contracts_count = (clan.rosters.select{ |c| c.start_date_time < Time.now && c.start_date_time > Time.now - 30.days }).count
  case relevant_contracts_count
  when 7..9999
    "Very Active"
  when 3..6
    "Moderately Active"
  when 1..2
    "Somewhat Active"
  when 0
    "Not Active"
  else
    "n/a"
  end
end

def average_age(clan)
  users = clan.users
  if users.count < 100
    pool_of_members = users
  else
    pool_of_members = users.sample(100)
  end

  ages = pool_of_members.map{|u| u.age}

  return (ages.reduce(:+).to_f / ages.size).round

end











