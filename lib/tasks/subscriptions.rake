namespace :subscriptions do
  desc "Create Standard Subcriptions"
  task :seed => :environment do
    SubscriptionPlan.create  name: "Monthly", price: 0.99, recurring: true, period: SubscriptionPlan.periods[:monthly] 
    SubscriptionPlan.create  name: "Yearly",  price: 4.99, recurring: true, period: SubscriptionPlan.periods[:yearly]  
  end
end
