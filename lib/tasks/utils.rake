namespace :utils do

	desc "Set Invoicing/Complete Status and send messages"
	task update_generosity: [:environment] do
    User.all.each do | user |
      user.update_generosity_rating!
    end
  end

	task generate_authentication_tokens: [:environment] do
		User.where(:authentication_token => nil).each do | u |
			u.ensure_authentication_token
			u.save
		end
	end

  desc "Update cancellation_rate for all users"
	task update_cancellation_rate: [:environment] do
    User.all.each do | user |
      user.update_cancellation_rate!
    end
  end

  desc "update users contracts_completed who have participated in rosters"
	task update_contracts_completed: [:environment] do
    Roster.where(status: 'Complete').each do | roster |
      roster.update_contracts_completed!
    end
  end
 desc "set logger to standard out"
  task :set_logger do
    logger = Logger.new(STDOUT)
    logger.level  = Logger::DEBUG
    Rails.logger = logger
  end
end
