require "test_helper"

class Contracts::ClaimedContractsControllerTest < ActionController::TestCase

  def claimed_contract
    @contract = contracts(:contract)
    @contract.update( status: "Complete")
    @contract
  end

  test "An unlogged in user should be redirected when trying to cancel an event" do
    post :destroy,  id: contracts(:one).id
    assert_redirected_to new_user_session_path
  end

  test "When a user cancels a contract their cancellation rate will increase" do
    @contract = claimed_contract
    @user = @contract.buyer

    @user.update_cancellation_rate!
    cancellations_should_be = @user.cancellations + 1
    cancellation_rate_was = @user.cancellation_rate 
    cancellation_reason = "Can not make it"

    sign_in @user
    post :destroy,  id: @contract.id, contract: { cancellation_reason: cancellation_reason }

    @contract.reload
    @user.reload

    assert_equal cancellation_reason, @contract.cancellation_reason
    assert_equal cancellations_should_be, @user.cancellations
    assert cancellation_rate_was <  @user.cancellation_rate
    assert_redirected_to events_path
  end

  test "When a user cancels a contract because the other user quits, the other players cancellation rate will increase" do
    @contract = claimed_contract
    @user = @contract.buyer
    @other = @contract.seller

    @other.update_cancellation_rate!
    cancellations_should_be = @other.cancellations + 1
    cancellation_rate_was = @other.cancellation_rate 
    cancellation_reason = "Mercenary Quit or Never Showed Up"

    sign_in @user
    post :destroy,  id: @contract.id, contract: { cancellation_reason: cancellation_reason }

    @contract.reload
    @other.reload

    assert_equal cancellations_should_be, @other.cancellations
    assert_equal cancellation_reason, @contract.cancellation_reason
    assert cancellation_rate_was <  @other.cancellation_rate
    assert_redirected_to events_path
  end

end
