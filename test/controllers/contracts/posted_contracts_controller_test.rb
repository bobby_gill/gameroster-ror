require "test_helper"

class Contracts::PostedContractsControllerTest < ActionController::TestCase

  def claimed_contract
    @contract = contracts(:contract)
    @contract.update( status: "Complete")
    @contract
  end

  test "An unlogged in user should be redirected when trying to cancel an event" do
    post :destroy,  id: contracts(:one).id
    assert_redirected_to new_user_session_path
  end

  test "When a user cancels a contract their cancellation rate will increase" do
    @contract = claimed_contract
    @user = @contract.seller

    @user.update_cancellation_rate!
    cancellations_should_be = @user.cancellations + 1
    cancellation_rate_was = @user.cancellation_rate 
    cancellation_reason = "Can not make it"

    sign_in @user
    post :destroy,  id: @contract.id, contract: { cancellation_reason: cancellation_reason }

    @contract.reload
    @user.reload

    assert_equal cancellations_should_be, @user.cancellations
    assert_equal cancellation_reason, @contract.cancellation_reason
    assert cancellation_rate_was <  @user.cancellation_rate
    assert_redirected_to events_path
  end

  test "When a user cancels a contract because the other user quits, the other players cancellation rate will increase" do
    @contract = claimed_contract
    @user = @contract.seller
    @other = @contract.buyer

    @other.update_cancellation_rate!
    cancellations_should_be = @other.cancellations + 1
    cancellation_rate_was = @other.cancellation_rate 
    cancellation_reason = "Player Quit or Never Showed Up"

    sign_in @user
    post :destroy,  id: @contract.id, contract: { cancellation_reason: cancellation_reason }

    @contract.reload
    @other.reload

    assert_equal cancellations_should_be, @other.cancellations
    assert_equal cancellation_reason, @contract.cancellation_reason
    assert cancellation_rate_was <  @other.cancellation_rate
    assert_redirected_to events_path
  end


  test "A LFP host should be able to mark the other player as a no show for an completed event." do
    @contract = contracts(:contract)
    @contract.update( status: "Complete")
    @user = users(:one)
    @buyer = @contract.buyer

    cancellations_should_be = @buyer.cancellations + 1
    cancellation_reason = "Player Quit or Never Showed Up"

    sign_in @user
    post :destroy,  id: @contract.id, contract: { cancellation_reason: cancellation_reason }

    @contract.reload

    assert_equal cancellations_should_be, @buyer.cancellations
    assert_equal cancellation_reason, @contract.cancellation_reason
    assert_redirected_to events_path
  end

  test "A LFP host for a complete event should see the NO SHOW button." do
    @contract = contracts(:contract)
    @user = users(:one)
 
    sign_in @user
    get :show,  id: @contract.id
    assert_select "input[name=commit][value='No Show']"

  end 

  test "A LFP host for a cancelled event should not see the NO SHOW button." do
    @contract = contracts(:cancelled_contract)
    @contract.update status:'Cancelled'

    @user = users(:one)
 
    sign_in @user
    get :show,  id: @contract.id

    assert_select "input[name=commit][value='No Show']", false
  end 

end
