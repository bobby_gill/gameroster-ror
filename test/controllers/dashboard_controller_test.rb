require 'test_helper'

class DashboardControllerTest < ActionController::TestCase

  test "As an anonymous user I should be redirected to sign in from the dashboard page" do
    get :index
    assert_redirected_to new_user_session_path
  end

  test "As a logged in user I should be view my dashboard page" do
    sign_in users(:one)
    get :index

    assert_response :success
  end

end
