require "test_helper"

class Bounties::PostedBountiesControllerTest < ActionController::TestCase

  def bounties( contract )
    contracts( contract ).becomes(Bounty)
  end

  test "An unlogged in user should be redirected when trying to cancel an event" do
    post :destroy,  id: bounties(:one).id
    assert_redirected_to new_user_session_path
  end

  test "When a user cancels a bounty because the other user quits, the other players cancellation rate will increase" do
    @bounty = bounties(:bounty)
    @other = @bounty.seller
    @user = @bounty.buyer

    @other.update_cancellation_rate!
    cancellations_should_be = @other.cancellations + 1
    cancellation_rate_was = @other.cancellation_rate 
    cancellation_reason = "Mercenary Quit or Never Showed Up"

    sign_in @user
    post :destroy,  id: @bounty.id, bounty: { cancellation_reason: cancellation_reason }

    @bounty.reload
    @other.reload

    assert_equal cancellations_should_be, @other.cancellations
    assert_equal cancellation_reason, @bounty.cancellation_reason
    assert  cancellation_rate_was <  @other.cancellation_rate
    assert_redirected_to events_path
  end

  test "When a user cancels a bounty their cancellation rate will increase" do
    @bounty = bounties(:bounty)
    @user = @bounty.buyer

    @user.update_cancellation_rate!
    cancellations_should_be = @user.cancellations + 1
    cancellation_rate_was = @user.cancellation_rate 
    cancellation_reason = "Can not make it"

    sign_in @user
    post :destroy,  id: @bounty.id, bounty: { cancellation_reason: cancellation_reason }

    @bounty.reload
    @user.reload

    assert_equal cancellations_should_be, @user.cancellations
    assert_equal cancellation_reason, @bounty.cancellation_reason
    assert cancellation_rate_was <  @user.cancellation_rate
    assert_redirected_to events_path
  end


end
