require "test_helper"

class Admin::CancellationsControllerTest < ActionController::TestCase

  test "An unlogged in user should be redirected when trying to view admin cancellations" do
    get :index
    assert_redirected_to new_admin_session_path
  end

  test "An logged in user should be redirected when trying to view admin cancellations" do
    sign_in users(:one)
    get :index
    assert_redirected_to new_admin_session_path
  end

  test "An admin should be able to view the index" do
    sign_in :admin, admins(:one)
    get :index
    assert_response :success
  end

  test "An admin reassigning a cancellation should make the buyers cancellation rate go up" do
    sign_in :admin, admins(:one)

    @contract = contracts(:cancelled_contract) 
    @buyer    = @contract.buyer
    @seller   = @contract.seller

    @buyer.update_cancellation_rate!
    cancellation_rate_was = @buyer.cancellation_rate 

    buyer_cancellations_should_be  = @buyer.cancellations + 1

    put :update, id: @contract.id, bounty: { admin_cancellation_note: "FOO", cancellation_assignee_id: @buyer.id, cancelation_reason: "BAR" }

    @buyer.reload
    
    assert_equal buyer_cancellations_should_be, @buyer.cancellations
    assert  cancellation_rate_was <  @buyer.cancellation_rate
  end

  test "An admin reassigning a cancellation should make the sellers cancellation rate go down" do
    sign_in :admin, admins(:one)

    @contract = contracts(:cancelled_contract) 
    @buyer    = @contract.buyer
    @seller   = @contract.seller

    @seller.update_cancellation_rate!
    cancellation_rate_was = @seller.cancellation_rate 

    seller_cancellations_should_be = @seller.cancellations - 1

    put :update, id: @contract.id, bounty: { admin_cancellation_note: "FOO", cancellation_assignee_id: @buyer.id, cancelation_reason: "BAR" }
    
    @seller.reload

    assert_equal seller_cancellations_should_be, @seller.cancellations
    assert  cancellation_rate_was >  @seller.cancellation_rate
  end

  test "An admin un-cancelling a full public roster should see the users cancellation rate go down  " do
    sign_in :admin, admins(:one)

    @roster = contracts(:full_public_roster).becomes( Roster )
    @roster.cancelled!
    @owner  = @roster.owner

    @owner.update_cancellation_rate!

    cancellation_rate_was = @owner.cancellation_rate 
    cancellations_should_be = @owner.cancellations - 1

    patch :uncancel, id: @roster.id
    
    @owner.reload

    assert_equal cancellations_should_be, @owner.cancellations
    assert  cancellation_rate_was >  @owner.cancellation_rate
  end
end
