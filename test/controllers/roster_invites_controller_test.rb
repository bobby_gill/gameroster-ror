require "test_helper"

class RosterInvitesControllerTest < ActionController::TestCase
  


  test "A user should be able to invite a single user to a roster" do
    sign_in users(:one)
    roster = contracts(:one)
    user = users(:two)
    post :create, {user_id: user.id, roster_id: roster.id, format: :js}
    assert_response :success
  end

  test "A user shouldn't be able invite a user who has already been invited" do 
    sign_in users(:one)
    roster = contracts(:full_public_roster)
    user = users(:two)
    post :create, {user_id: user.id, roster_id: roster.id, format: :js}
    assert_response :unprocessable_entity
  end
end
