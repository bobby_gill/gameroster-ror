require "test_helper"

class Profile::NotificationsControllerTest < ActionController::TestCase

  test "An unlogged in user should be redirected when trying to edit their notification settings" do
    get :edit
    assert_redirected_to new_user_session_path
  end

  test "A user should be able to edit their notification settings" do
    sign_in users(:one)
    get :edit
    assert_response :success
  end


end
