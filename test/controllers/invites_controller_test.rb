require "test_helper"

class InvitesControllerTest < ActionController::TestCase
  def invite
    @invite ||= invites(:one)
  end

  test "as an user i should not be able to destroy an invite that I did not create" do
    sign_in users(:two)
    assert_difference('Invite.count', 0) do
      delete :destroy, id: invite
    end

  end


  test "as a host i should be able to destroy an invite" do

    sign_in users(:one)
    assert_difference('Invite.count', -1) do
      delete :destroy, id: invite
    end

  end

  test "A user should be able to join a public roster" do
    @roster = contracts(:public_roster)
    sign_in users(:three)

    assert_difference('Invite.count', 1) do
      post :create, roster_id: @roster.id
    end
  end

  test "A user joining a public roster should send the host a message" do
    @roster = contracts(:public_roster)
    sign_in users(:three)

    assert_difference('Mailboxer::Message.count',1) do
      post :create, roster_id: @roster.id
    end
  end

  test "A host marking a user a no show on a full public roster should increase that users cancellation rate" do
    sign_in users(:one)

    roster = contracts(:full_public_roster).becomes(Roster)
    roster.update( status: "Complete")

    user = users(:two)
    invite = user.invites.find_by( contract_id:  roster.id )

    user.update_cancellation_rate!

    cancellations_should_be = user.cancellations + 1
    cancellation_rate_before = user.cancellation_rate

    post  :no_show, id: invite.id

    user.reload

    assert_equal cancellations_should_be, user.cancellations
    assert  cancellation_rate_before < user.cancellation_rate
  end

  test "A host reconfirming a user that was a no show on a full public roster should decrease that users cancellation rate" do
    sign_in users(:one)

    roster = contracts(:full_public_roster).becomes(Roster)
    roster.update( status: "Complete")

    user = users(:two)

    invite = user.invites.find_by( contract_id:  roster.id )
    invite.no_show!

    user.update_cancellation_rate!

    cancellations_should_be = user.cancellations - 1
    cancellation_rate_before = user.cancellation_rate

    post  :reconfirm, id: invite.id

    user.reload

    assert_equal cancellations_should_be, user.cancellations
    assert  cancellation_rate_before > user.cancellation_rate
  end

end
