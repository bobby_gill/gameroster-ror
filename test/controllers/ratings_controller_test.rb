require "test_helper"

class RatingsControllerTest < ActionController::TestCase

  test "A unlogged in user should be redirected when trying to review an event" do
    post :create, profile_id: users(:one).id, rating: ratings(:one).attributes, format: 'js'
    assert_response 401
  end

  test "A user should be able to rate another user whom they have shared an event" do
    sign_in users(:two)
    post :create, profile_id: users(:one).id, rating: ratings(:one).attributes, format: 'js'
    assert_response :success
  end

  test "A user will need to be rated by 3 different people before having their ratings updated" do

    rated_user = users(:one)

    [ :two, :three, :four ].each_with_index do |u, i| 
      sign_in users(u)
      assert_difference('Rating.count') do
        post :create, profile_id: rated_user.id, rating: { personality_rating: 5, approval_rating: 5, skill_rating: 5 }, format: 'js'
      end
      sign_out users(u)
    end

    rated_user.reload
    assert rated_user.psa_rating > 0, rated_user.psa_rating

  end


end
