require "test_helper"

class Api::V1::DevicesControllerTest < ActionController::TestCase
  def setup
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s
    ENV['PARSE_ENABLED'] = "true"
  end

  def api_sign_in user
    @request.headers["X-User-Token"] =  user.authentication_token
    @request.headers["X-User-Email"] =  user.email
  end

  def json_response
    ActiveSupport::JSON.decode @response.body
  end

  test "An api user can register their device" do
    user = users(:one)
    api_sign_in user

    device_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

    assert_difference('Device.count') do
      post :create, device: { 
        device_token:  device_token,
        device_type: "ios" 
      }
    end
  end

  test "An api user can register their device even if the token has been assigned to another user" do
   
    user = users(:one)
    api_sign_in user
    device_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

    users(:two).devices.create( device_token: device_token, device_type: "ios" )

    assert_no_difference('Device.count') do
      post :create, device: { 
        device_token: device_token,
        device_type: "ios" 
      }
    end

    assert_response :ok
    assert_includes user.devices.pluck(:device_token), device_token
  end

end
