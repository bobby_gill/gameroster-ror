require "test_helper"

class Api::V1::RostersControllerTest < ActionController::TestCase
  def setup
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s
  end

  def api_sign_in user
    @request.headers["X-User-Token"] =  user.authentication_token
    @request.headers["X-User-Email"] =  user.email
  end

  def json_response
    ActiveSupport::JSON.decode @response.body
  end

  def private_roster
    @private_roster ||= Roster.find(contracts(:private_roster).id)
  end

  def public_roster
    @public_roster ||= Roster.find(contracts(:public_roster).id)
  end

  test "an anonymous should not see the index" do
    get :index
    assert_response :unauthorized
  end

  test "an api user not be able to see the index with the wrong token" do
    user = users(:one)
    user.authentication_token = "wrong_token"
    api_sign_in(user)
    get :index
    assert_response :unauthorized
  end

  test "an api user should see the index" do
    user = users(:one)
    api_sign_in(user)
    get :index
    assert_response :ok
  end

  test "an anonymous should not see a private roster" do
    get :show, id: private_roster
    assert_response :unauthorized
  end

  test "An uninvited user should be able to view a public roster" do
    api_sign_in users(:two)
    get :show, id: public_roster
    assert_response :ok
  end

  test "An uninvited user should be redirected from a private event" do
    api_sign_in users(:two)
    get :show, id: private_roster
    assert_response :unauthorized
  end

  test "An invited user should be able to view a private event" do
    private_roster.user_ids = users(:two).id
    api_sign_in users(:two)
    get :show, id: private_roster
    assert_response :ok
  end

  test "An api user should be able to create a roster with just required attributes" do
    user = users(:one)

    api_sign_in user

    assert_difference('Roster.count') do
      post :create, contract: { title: "title", game_game_system_join_ids: user.game_game_system_joins.first.id, duration: "60", start_date_time: 1.day.from_now, play_type: "Casual", will_play: "All Types", max_roster_size: 10, waitlist: "1", private: "0"   }
    end

  end

  test "An api user should receive a list of  when submitting an incomplete roster" do
    api_sign_in users(:one)

    post :create, contract: { }

    assert_response :unprocessable_entity
    assert_includes json_response, "will_play"
  end

  test "An api user should be able to create a roster with a list of user_ids to invite" do
    user = users(:one)

    api_sign_in user

    user_ids = [ users(:two).id, users(:three).id ].join(',')

    assert_difference('Invite.count', 2) do
      post :create, contract: { title: "An api user should be able to create a roster with a list of user_ids to invite", game_game_system_join_ids: user.game_game_system_joins.first.id, duration: "60", start_date_time: 1.day.from_now, play_type: "Casual", will_play: "All Types", max_roster_size: 10, waitlist: "1", private: "0", user_ids: user_ids }
    end

  end

  test "An api user should return a list of errors when passing an incomplete roster" do
    @roster = public_roster
    @user = @roster.owner
    api_sign_in @user
    put :update, id: @roster.id, contract: @roster.attributes.merge( title: nil )

    assert_response :unprocessable_entity
    assert_includes json_response, "title"
  end

  test "An api user should be able to update a roster" do
    @roster = public_roster
    @user = @roster.owner
    api_sign_in @user
    put :update, id: @roster.id, contract: @roster.attributes.merge( title: 'Z' )

    assert_response 204
  end

end
