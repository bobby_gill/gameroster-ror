require "test_helper"

class DonationsControllerTest < ActionController::TestCase
  def donation
    @donation ||= donations(:one)
  end

  def donatee
    @donatee ||= users(:two)
  end

  def test_new
    sign_in users(:one)
    get :new, profile_id: donatee.username
    assert_response :success
  end

end
