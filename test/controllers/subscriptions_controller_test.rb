require "test_helper"

class SubscriptionsControllerTest < ActionController::TestCase
  test "A unlogged in user should be redirected" do
    get :new, subscription_plan_id: subscription_plans(:one)
    assert_response :redirect
  end

  test "logged in user should be able to view cancel" do
    sign_in users(:one)
    get :cancel, token: 'EC-token'
    assert_redirected_to profile_subscription_path
  end


  test "subscription/cancel takes you to to cancel action and not show" do
    assert_routing '/subscriptions/cancel', controller: "subscriptions", action: "cancel"
  end

end
