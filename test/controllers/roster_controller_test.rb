require "test_helper"

class RostersControllerTest < ActionController::TestCase

  def private_roster
    @private_roster ||= Roster.find(contracts(:private_roster).id)
  end

  def public_roster
    @public_roster ||= Roster.find(contracts(:public_roster).id)
  end

  test "A unlogged in user should be redirected when viewing a private roster" do
    get :show, id: private_roster
    assert_response :redirect
  end

  test "A unlogged in user should be redirected when viewing a public roster" do
    get :show, id: public_roster
    assert_response :redirect
  end

  test "An uninvited user should be able to view a public roster" do
    sign_in users(:two)
    get :show, id: public_roster
    assert_response :success
  end

  test "An uninvited user should be redirected from a private event" do
    sign_in users(:two)
    get :show, id: private_roster
    assert_response :redirect
  end

  test "An invited user should be able to view a private event" do
    private_roster.user_ids = users(:two).id
    sign_in users(:two)
    get :show, id: private_roster
    assert_response :success
  end

  test "A roster host should be able to cancel a public roster" do
    @roster = public_roster
    @user = @roster.owner

    cancelled_rosters_count_should_be = @user.cancelled_rosters.count + 1

    sign_in @user
    post :destroy, id: @roster.id

    assert_equal cancelled_rosters_count_should_be, @user.cancelled_rosters.count

  end

  test "A user should be able to create a public roster" do
    user = users(:one)
    sign_in user

    assert_difference('Roster.count') do
      post :create, roster: { title: "title", game_game_system_join_ids: user.game_game_system_joins.first.id, duration: "60", start_date_time: 1.day.from_now, play_type: "Casual", will_play: "All Types", max_roster_size: 10, waitlist: "1", private: "0"   }
    end
  end

  test "A user should be able to create a public roster with a duration greater than an hour" do
    user = users(:one)
    sign_in user

    post :create, roster: { title: "title", game_game_system_join_ids: user.game_game_system_joins.first.id, duration: "120", start_date_time: 1.day.from_now, play_type: "Casual", will_play: "All Types", max_roster_size: 10, waitlist: "1", private: "0"   }

    @roster = assigns(:roster)

    assert_equal @roster.start_date_time + 2.hour,  @roster.end_date_time
  end



end
