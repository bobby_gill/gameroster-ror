require "test_helper"

class Clans::ClansControllerTest < ActionController::TestCase

  setup do
    # Kludge until we figure out a way to mark these relations in the fixture

    clan = clans(:one)

    [ :clan_contract_2, :clan_contract_1 ].each do | clan_contract |
      contracts( clan_contract ).update_attribute :clan_id, clan.id
    end

    Rails.logger.info "********* SETUP"

  end

  def clan
    clan ||= clans(:one)
  end

  def test_index
    sign_in users(:one)
    get :index
    assert_response :success
    assert_not_empty assigns(:clans)
  end

  def test_filter_with_games
    sign_in users(:one)
    get :index, select_filter: { game: [ games(:one), games(:two) ] }
    assert_response :success
    assert_not_empty assigns(:clans)
  end

  def test_new
    sign_in users(:one)
    get :new
    assert_response :success
  end

  def test_create
    sign_in users(:one)

    assert_difference("Clan.count") do
      post :create, clan: { minimum_age: clan.minimum_age, availability: clan.availability, bio: clan.bio, family_friendly: clan.family_friendly,  game_type: clan.game_type, ground_rules: clan.ground_rules, languages: clan.languages, most_active: clan.most_active, name: clan.name, play_style: clan.play_style }

      # byebug
      assert_redirected_to clan_path(assigns(:clan))
    end
  end

  def test_show
    # Not signed in
    clan.update(host_id: users(:one).id)
    get :show, id: clan
    assert_response :redirect

    # Signed in
    sign_in users(:one)
    get :show, id: clan
    assert_response :success
  end

  def test_edit
    # Not signed in
    get :edit, id: clan
    assert_response :redirect

    # Signed in
    sign_in users(:one)
    get :edit, id: clan
    assert_response :redirect

    # Signed in as host
    clan.update(host_id: users(:one).id)
    get :edit, id: clan
    assert_response :success
  end

  def test_update
    def update_clan
      put :update, id: clan, clan: { activity_level: clan.activity_level, minimum_age: clan.minimum_age, availability: clan.availability, bio: clan.bio, family_friendly: clan.family_friendly,  game_type: clan.game_type, ground_rules: clan.ground_rules, languages: clan.languages, most_active: clan.most_active, name: "superclan", play_style: clan.play_style }
    end

    # Not signed in
    update_clan
    clan.reload
    assert_not_equal "superclan", clan.name
    assert_redirected_to new_user_session_path

    # Signed in
    sign_in users(:one)
    update_clan
    clan.reload
    assert_not_equal "superclan", clan.name
    assert_redirected_to clan

    # Signed in as host
    clan.update(host_id: users(:one).id)
    update_clan
    clan.reload
    assert_equal "superclan", clan.name
    assert_redirected_to clan_path(assigns(:clan))
  end

  def test_destroy
    # A clan can't be destroyed without a user
    assert_difference("Clan.count", 0) do
      delete :destroy, id: clan
      assert_redirected_to new_user_session_path
    end
    # A clan can't be destroyed without it's host
    sign_in users(:one)
    assert_difference("Clan.count", 0) do
      delete :destroy, id: clan
      assert_redirected_to clan_path(clan)
    end
    # A clan can be destroyed by it's host
    clan.update(host_id: users(:one).id)
    assert_difference("Clan.count", -1) do
      delete :destroy, id: clan
      assert_redirected_to clans_path
    end
  end

end
