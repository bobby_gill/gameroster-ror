require "test_helper"

class Clans::ClanRanksControllerTest < ActionController::TestCase

  # Makes two clan ranks (default)
  def clan_ranks
    clans(:one).init_ranks
    clan_ranks ||= clans(:one).clan_ranks
  end

  def test_sanity
    assert true
  end

  # def test_new
  #   sign_in users(:one)
  #   get :new, :clan_id => 1
  #   assert_response :success
  # end
  # 
  def test_create
    assert_difference("ClanRank.count", 0) do
      post :create, clan_rank: {clan_id: clans(:one).id, title: "buffoon", level: 3}, clan_id: clans(:one).id
    end

    sign_in users(:one)
    assert_difference("ClanRank.count", 1) do
      post :create, clan_rank: {clan_id: clans(:one).id, title: "buffoon", level: 3}, clan_id: clans(:one).id
    end
    assert_redirected_to clan_path(clans(:one))
  end

  def test_index
    sign_in users(:one)
    get :index, clan_id: clans(:one).id
    assert_response :success
    assert_not_nil assigns(:clan)
    assert_not_nil assigns(:ranks)
  end

  def test_update
    def update_clan_rank rank
      put :update, id: rank, clan_rank: {permissions: [:invite_users]}
    end

    rank = clan_ranks.first
    update_clan_rank rank
    rank.reload
    assert_not rank.permissions?(:invite_users)

    sign_in users(:one)
    update_clan_rank rank
    rank.reload
    assert rank.permissions?(:invite_users)
  end

  def test_destroy
    rank = clan_ranks.second
    assert_difference("ClanRank.count", 0) do
      delete :destroy, id: rank
      assert_redirected_to new_user_session_path
    end

    sign_in users(:one)
    assert_difference("ClanRank.count", -1) do
      delete :destroy, id: rank
      assert_redirected_to clan_clan_ranks_path(clans(:one))
    end
  end
end
