require "test_helper"

class Clans::ClanInvitesControllerTest < ActionController::TestCase

  def clan_invite
    clan_invite ||= clan_invites(:one)
  end

  def accept_clan_invite clan_invite
    put :update, id: clan_invite, clan_invite: {status: "confirmed"}
  end

  def deny_clan_invite clan_invite
    put :update, id: clan_invite, clan_invite: {status: "declined"}
  end

  def test_sanity
    assert true
  end

  def test_create
    assert_difference("ClanInvite.count", 0) do
      post :create, clan_invite: {clan_id: clans(:one).id, user_id: users(:one), inviter_id: users(:two)}
    end

    sign_in users(:one)
    assert_difference("ClanInvite.count", 1) do
      post :create, clan_invite: {clan_id: clans(:one).id, user_id: users(:one), inviter_id: users(:two)}
    end
    assert_redirected_to clan_path(clans(:one))
  end


  def test_update
    clan_invite = ClanInvite.create(user: users(:one), clan: clans(:one), inviter: users(:one))

    accept_clan_invite clan_invite
    clan_invite.reload
    assert_not clan_invite.status == "confirmed"

    sign_in users(:one)
    accept_clan_invite clan_invite
    clan_invite.reload
    assert clan_invite.status == "pending"
  end

  def test_deny_a_join_request_if_you_are_host
    sign_in users(:two)
    clans(:one).update(host_id: users(:two).id)
    clan_invite = ClanInvite.create(user: users(:one), clan: clans(:one), inviter: users(:one))
    clans(:one).update(host_id: users(:two).id)
    clan_invite.reload
    assert_difference("ClanInvite.count", 0) do 
      assert_difference("ClanMember.count", 0) do
        deny_clan_invite clan_invite
        clan_invite.reload
      end
    end
  end

  def test_accept_a_join_request_if_you_are_host
    sign_in users(:two)
    clans(:one).update(host_id: users(:two).id)
    clan_invite = ClanInvite.create(user: users(:one), clan: clans(:one), inviter: users(:one))
    assert_difference("ClanInvite.count", -1) do 
      assert_difference("ClanMember.count", 1) do
        accept_clan_invite clan_invite
      end
    end
    sign_out users(:two)
  end

  def test_accept_an_invite_from_another_user
    sign_in users(:one)
    clan_invite = ClanInvite.create(user: users(:one), clan: clans(:one), inviter: users(:two))
    assert_difference("ClanInvite.count", -1) do 
      assert_difference("ClanMember.count", 1) do
        accept_clan_invite clan_invite
      end
    end
  end

  def test_decline_an_invite_from_another_user
    sign_in users(:one)
    clan_invite = ClanInvite.create(user: users(:one), clan: clans(:one), inviter: users(:two))
    assert_difference("ClanInvite.count", 0) do 
      assert_difference("ClanMember.count", 0) do
        deny_clan_invite clan_invite
        clan_invite.reload
      end
    end
  end

end
