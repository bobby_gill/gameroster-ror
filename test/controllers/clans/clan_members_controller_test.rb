require "test_helper"

class Clans::ClanMembersControllerTest < ActionController::TestCase

  def clan_member
    clan_member ||= ClanMember.create(clan: clans(:one), user: users(:one))
  end

  def create_member invite_id=nil
    post :create, clan_member: {clan_id: clans(:one).id, user: users(:one)}, clan_id: clans(:one).id, invite_id: invite_id
  end

  def test_create_when_clan_is_not_private_with_autojoin
    clans(:one).update(private: false, autojoin: true)
    assert_difference("ClanMember.count", 0) do
      create_member
    end

    sign_in users(:one)
    assert_difference("ClanMember.count", 1) do
      create_member
    end
    assert_redirected_to clan_path(clans(:one))
  end

  def test_create_when_clan_is_not_private_with_no_autojoin
    invite = ClanInvite.create(clan: clans(:one), user: users(:one), inviter: users(:two))
    invite.update(status: "confirmed")
    clans(:one).update(private: false, autojoin: false)

    sign_in users(:one)
    assert_difference("ClanMember.count", 1) do
      create_member invite.id
    end
    assert_redirected_to clan_path(clans(:one))
  end

  def test_create_when_clan_is_private_with_no_autojoin
    invite = ClanInvite.create(clan: clans(:one), user: users(:one), inviter: users(:two))
    invite.update(status: "confirmed")
    clans(:one).update(private: true, autojoin: false)

    sign_in users(:one)
    assert_difference("ClanMember.count", 1) do
      create_member invite.id
    end
    assert_redirected_to clan_path(clans(:one))
  end

  def test_create_when_clan_invite_is_not_approved
    invite = ClanInvite.create(clan: clans(:one), user: users(:one), inviter: users(:two))
    clans(:one).update(private: false, autojoin: false)

    sign_in users(:one)
    assert_difference("ClanMember.count", 0) do
      create_member invite.id
    end
    assert_redirected_to clan_path(clans(:one))
  end

  def test_index
    get :index, clan_id: clans(:one).id
    assert_redirected_to new_user_session_path
    sign_in users(:one)
    get :index, clan_id: clans(:one).id
    assert_response :success
  end

  def test_edit
    sign_in users(:one)
    member = clan_member
    get :edit, id: member #couldn't test ajax due to CSRF ISSUE
    assert_response :success
  end

  def update_clan_member member
    put :update, id: member, clan_member: {clan_rank_id: member.clan.clan_ranks.second.id}
  end

  def test_update
    member = clan_member
    clan = clans(:one)
    clan.init_ranks

    update_clan_member member
    member.reload
    assert_not member.clan_rank_id == member.clan.clan_ranks.second.id

    sign_in users(:one)

    update_clan_member member
    member.reload
    assert member.clan_rank_id == member.clan.clan_ranks.second.id
  end

  def test_destroy_not_signed_in
    member = clan_member
    assert_difference("ClanMember.count", 0) do
      delete :destroy, id: member
    end
  end

  def test_destroy_signed_in_with_not_user_or_host
    member = clan_member
    sign_in users(:two)
    assert_difference("ClanMember.count", 0) do
      delete :destroy, id: member
    end
    sign_out users(:two)
  end

  def test_destroy_signed_in_with_user
    member = clan_member
    sign_in users(:one)
    assert_difference("ClanMember.count", -1) do
      delete :destroy, id: member
    end
  end

  def test_destroy_signed_in_with_host
    member = clan_member
    sign_in users(:two)
    clans(:one).update(host_id: users(:two).id)
    assert_difference("ClanMember.count", -1) do
      delete :destroy, id: member
    end
  end

end
