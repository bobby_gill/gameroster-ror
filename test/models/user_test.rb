require "test_helper"

class UserTest < ActiveSupport::TestCase
  def user
    @user ||= users(:one)
  end

  def rosters contract
    Roster.find( contracts(contract).id )
  end

  def test_valid
    assert user.valid?, user.errors.full_messages
  end

  test "should require a country when do_country_validation is set" do
      new_user = user
      new_user.country = nil
      user.do_country_validation = true
      assert_not user.valid?
      assert user.errors[:country].any?
  end

  test "should require an ISO 3611 country code when do_country_validation is set" do
      new_user = user
      new_user.country = "XXX"
      user.do_country_validation = true
      assert_not user.valid?
      assert user.errors[:country].any?
  end

  test "should not use paypal if from Puerto Rico" do
      new_user = user
      new_user.country = "PR"
      assert_not user.paypalable?
  end

  test "should use paypal if country is nil" do
      new_user = user
      new_user.country = nil
      assert user.paypalable?
  end

  test "should use paypal if country is present but not a legit code" do
      new_user = user
      new_user.country = 'XXX'
      assert user.paypalable?
  end

  test "should not use paypal if from the US" do
      new_user = user
      new_user.country = "US"
      assert user.paypalable?
  end

  test "experience should increase when completing a public roster event that I own" do
      experience_should_be = user.experience + 1
      
      @contract = rosters(:public_roster)
      @contract.update_attribute( :status, "Complete")

      assert_equal experience_should_be, user.experience
  end

  test "experience should not increase when completing a private roster event" do
      experience_should_be = user.experience
      
      @contract = rosters(:private_roster)
      @contract.update_attribute( :status, "Complete")

      assert_equal experience_should_be, user.experience
  end


  test "experience should increase when completing a public roster event that I have been invited to and have been confirmed" do
      user = users(:two)
      experience_should_be = user.experience + 1
      
      @contract = rosters(:public_roster)
      @contract.update_attribute( :status, "Complete")
      @invite = user.invites.find_by( contract_id:  @contract.id )
      @invite.confirmed!

      assert_equal experience_should_be, user.experience
  end

  test "experience should not increase when completing a public roster event that I have been invited to and have not been confirmed" do
      user = users(:two)
      experience_should_be = user.experience
      
      @contract = rosters(:public_roster)
      @contract.update_attribute( :status, "Complete")
      @invite = user.invites.find_by( contract_id:  @contract.id )

      @invite.pending!
      assert_equal experience_should_be, user.experience

      @invite.declined!
      assert_equal experience_should_be, user.experience

      @invite.waitlisted!
      assert_equal experience_should_be, user.experience

      @invite.no_show!
      assert_equal experience_should_be, user.experience
  end

  test "cancellations should increase when cancelling a full public event" do
      cancellations_should_be = user.cancellations + 1
      
      @contract = rosters(:full_public_roster)
      @contract.cancelled!

      assert_equal cancellations_should_be, user.cancellations
  end

  test "cancellations should not increase when cancelling a partially full public event" do
      cancellations_should_be = user.cancellations
      
      @contract = rosters(:public_roster)
      @contract.cancelled!

      assert_equal cancellations_should_be, user.cancellations
  end

  test "cancellations should not increase when cancelling a full private event" do
      cancellations_should_be = user.cancellations
      
      @contract = rosters(:full_public_roster)
      @contract.update( private: true)
      @contract.cancelled!

      assert_equal cancellations_should_be, user.cancellations
  end

  test "cancellations should increase when a user's invitation has been marked a no_show for a full completed public event" do
      user = users(:two)
      cancellations_should_be = user.cancellations + 1
      
      @contract = rosters(:full_public_roster)
      @contract.update_attribute( :status, "Complete")
      @invite = user.invites.find_by( contract_id:  @contract.id )
      @invite.no_show!

      assert_equal cancellations_should_be, user.cancellations
  end

  test "cancellations should not increase when a user's invitation has been marked a no_show for a partially completed public event" do
      user = users(:two)
      cancellations_should_be = user.cancellations 
      
      @contract = rosters(:public_roster)
      @contract.update_attribute( :status, "Complete")
      @invite = user.invites.find_by( contract_id:  @contract.id )
      @invite.no_show!

      assert_equal cancellations_should_be, user.cancellations
  end

end
