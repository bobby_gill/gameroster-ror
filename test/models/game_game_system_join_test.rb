require "test_helper"

class GameGameSystemJoinTest < ActiveSupport::TestCase
  def game_game_system_join
    @game_game_system_join ||= GameGameSystemJoin.new
  end

  def test_valid
    assert game_game_system_join.valid?
  end
end
