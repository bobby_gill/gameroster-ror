require "test_helper"

class GameTest < ActiveSupport::TestCase
  def game
    @game ||= Game.new( title: "foo")
  end

  def test_valid
    assert game.valid?, game.errors.full_messages
  end
end
