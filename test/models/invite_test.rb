require "test_helper"

class InviteTest < ActiveSupport::TestCase
  def invite
    @slot ||= Invite.new( user: users(:one), contract: contracts(:one) )
  end

  def test_valid
    assert invite.valid?
  end
end
