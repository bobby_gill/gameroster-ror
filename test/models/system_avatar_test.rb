require "test_helper"

class SystemAvatarTest < ActiveSupport::TestCase
  def system_avatar
    @system_avatar ||= SystemAvatar.new
  end

  def test_valid
    assert system_avatar.valid?
  end
end
