require "test_helper"

class RosterMessageTest < ActiveSupport::TestCase

  def roster
    @roster ||= Roster.find( contracts(:one).id )
  end

  def roster_message
    @roster_message ||= RosterMessage.new( user: users(:one), roster: roster, message: "123" )
  end

  def test_valid
    assert roster_message.valid?
  end
end
