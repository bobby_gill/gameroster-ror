require "test_helper"

class ClanTest < ActiveSupport::TestCase
  def clan
    @clan ||= Clan.new
  end

  def test_valid
    assert clan.valid?
  end

  def test_member
    # User isn't a member
    assert_not clans(:two).member users(:one)

    # User is a member
    assert_difference("ClanMember.count", 1) do
      ClanMember.create(user: users(:one), clan: clans(:two))
    end

    assert clans(:two).member users(:one)
  end

  def test_host
    assert_not clans(:two).is_host? users(:one)

    clans(:two).update(host_id: users(:one).id)
    assert clans(:two).is_host? users(:one)
  end

end
