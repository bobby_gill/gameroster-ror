require "test_helper"

class DonationPaypalLogTest < ActiveSupport::TestCase
  def donation_paypal_log
    @donation_paypal_log ||= DonationPaypalLog.new
  end

  def test_valid
    assert donation_paypal_log.valid?
  end
end
