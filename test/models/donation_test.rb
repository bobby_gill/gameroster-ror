require "test_helper"

class DonationTest < ActiveSupport::TestCase
  def donation
    @donation ||= Donation.new
  end

  def test_valid
    @donation = Donation.new( amount_cents: 100 )
    assert donation.valid?
  end
end
