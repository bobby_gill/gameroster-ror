require "test_helper"

class GameSystemTest < ActiveSupport::TestCase
  def game_system
    @game_system ||= GameSystem.new( title: "title", abbreviation: "abbr" )
  end

  def test_valid
    assert game_system.valid?, game_system.errors.full_messages
  end
end
