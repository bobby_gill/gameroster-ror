require "test_helper"

class ClanGameTest < ActiveSupport::TestCase
  def clan_game
    @clan_game ||= ClanGame.new
  end

  def test_valid
    assert clan_game.valid?
  end
end
