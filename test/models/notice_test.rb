require "test_helper"

class NoticeTest < ActiveSupport::TestCase
  def notice
    @notice ||= notices(:one)
  end

  def test_valid
    assert notice.valid?
  end

  test "Notice should not save without title" do
    n = notice
    n.title = nil
    assert_not n.valid?, "Notice saved without title."
  end

  test "Notice should not save without body" do
    n = notice
    n.body = nil
    assert_not n.valid?, "Notice saved without body."
  end
end
