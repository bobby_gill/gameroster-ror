require "test_helper"

class ContactRequestTest < ActiveSupport::TestCase
  def contact_request
    @contact_request ||= ContactRequest.new( name: "Name", message: "message", contact_type: "contact_type", email: "email@example.com" )
  end

  def test_valid
    assert contact_request.valid?, contact_request.errors.full_messages
  end
end
