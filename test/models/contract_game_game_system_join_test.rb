require "test_helper"

class ContractGameGameSystemJoinTest < ActiveSupport::TestCase
  def contract_game_game_system_join
    @contract_game_game_system_join ||= ContractGameGameSystemJoin.new
  end

  def test_valid
    assert contract_game_game_system_join.valid?
  end
end
