require "test_helper"

class AnnouncementTest < ActiveSupport::TestCase
  def announcement
    @announcement ||= Announcement.new
  end

  def test_valid
    assert announcement.valid?
  end
end
