require "test_helper"

class ContractTest < ActiveSupport::TestCase
  def contract
    @contract ||= Contract.new
  end

  def rosters contract
    Roster.find( contracts(contract).id )
  end

  test "private contract is valid" do
      @contract = contracts(:private_roster)
      assert @contract.valid?, @contract.errors.full_messages
  end

  test "public contract is valid" do
      @contract = contracts(:public_roster)
      assert @contract.valid?, @contract.errors.full_messages
  end

  test "full public contract is valid" do
      @contract = rosters(:full_public_roster)
      assert @contract.valid?, @contract.errors.full_messages
  end

  test "when a roster is cancelled was_full will be updated to reflect that the event was full"  do
    @contract = rosters(:public_roster)
    @contract.update( was_full: nil )
    @contract.max_roster_size = 0
    @contract.cancelled!
    assert_equal true, @contract.was_full
  end

  test "when a roster is cancelled was_full will be updated to reflect that the event was not full"  do
    @contract = rosters(:public_roster)
    @contract.update( was_full: nil )
    @contract.max_roster_size = 1_000
    @contract.cancelled!
    assert_equal false, @contract.was_full
  end

  test "when a roster is complete was_full will be updated to reflect that the event was full"  do
    @contract = rosters(:public_roster)
    @contract.update( was_full: nil )
    @contract.max_roster_size = 0
    @contract.complete!
    assert_equal true, @contract.was_full
  end

  test "when a roster is complete was_full will be updated to reflect that the event was not full"  do
    @contract = rosters(:public_roster)
    @contract.update( was_full: nil )
    @contract.max_roster_size = 1_000
    @contract.complete!
    assert_equal false, @contract.was_full
  end


end
