require "test_helper"

class ContractPaypalLogTest < ActiveSupport::TestCase
  def contract_paypal_log
    @contract_paypal_log ||= ContractPaypalLog.new
  end

  def test_valid
    assert contract_paypal_log.valid?
  end
end
