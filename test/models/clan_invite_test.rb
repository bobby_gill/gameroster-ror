require "test_helper"

class ClanInviteTest < ActiveSupport::TestCase
  def clan_invite
    @clan_invite ||= ClanInvite.new
  end

  def test_valid
    assert ClanInvite.first.valid?
  end
end
