require "test_helper"

class RatingTest < ActiveSupport::TestCase
  def rating
    @rating ||= Rating.new
  end

  def test_valid
    rating.update_attributes(
      user: users(:one),
      rated_user:  users(:two),
      personality_rating: 1,
      approval_rating: 1,
      skill_rating: 1
    )
    assert rating.valid?, rating.errors.full_messages
  end

end
