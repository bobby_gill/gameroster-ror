require "test_helper"

class SubscriptionPlanTest < ActiveSupport::TestCase
  def subscription_plan
    @subscription_plan ||= SubscriptionPlan.new( subscription_plans(:one).attributes )
  end

  def test_valid
    assert subscription_plan.valid?, subscription_plan.errors.full_messages
  end
end
