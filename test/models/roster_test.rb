require "test_helper"

class RosterTest < ActiveSupport::TestCase
  def user
    @user ||= users(:one)
  end

  def rosters contract
    contracts(contract).becomes(Roster)
  end

  def test_valid
    @roster = rosters(:full_public_roster)
    assert @roster.valid?, @roster.errors.full_messages
  end

  test "A roster should return a list of users that meet the owners contract preferences" do
    @roster = rosters(:full_public_roster)
    user = users(:two)
    assert_includes @roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.ids, user.id
  end

  test "A private roster should never return a list of users that meet the owners contract preferences" do
    @roster = rosters(:private_roster)
    assert_equal 0, @roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.count
  end

  test "A roster should not return a user that does not have the rosters game or system" do
    @roster = rosters(:full_public_roster)
    user = users(:two)
    user.game_game_system_joins = [ game_game_system_joins(:one) ]
    user.save

    assert_not_includes @roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.ids, user.id
  end

  test "A roster should not return a user that has their notification settings disabled " do
    @roster = rosters(:full_public_roster)
    user = users(:two)
    user.update( notif_games: false )

    assert_not_includes @roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.ids, user.id
  end

  test "A roster should not return a user that has the rosters owner blocked" do
    @roster = rosters(:full_public_roster)
    user = users(:two)
    user.blocked_users << @roster.owner

    assert_not_includes @roster.users_that_meet_contract_preferences_and_have_the_same_game_and_system.ids, user.id
  end

end
