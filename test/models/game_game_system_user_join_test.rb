require "test_helper"

class GameGameSystemUserJoinTest < ActiveSupport::TestCase
  def game_game_system_user_join
    @game_game_system_user_join ||= GameGameSystemUserJoin.new
  end

  def test_valid
    assert game_game_system_user_join.valid?
  end
end
